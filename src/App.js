import logo from './logo.svg';
import { Routes, Route } from 'react-router-dom';
import Home from './pages/Home';
import Shop from './pages/Shop';
import Cart from './pages/Cart';
import Checkout from './pages/Checkout';
import Confirm from './pages/Confirm';
import Single from './pages/Single';
import Register from './pages/Register';
import Login from './pages/Login';
import { ToastContainer } from 'react-toastify';
import { useEffect } from 'react';
import './firebase_init_messaging';
import Profile from './pages/profile';

function App() {
 
  return (
    <>
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/shop" element={ <Shop/>} />
      <Route path="/cart" element={ <Cart/>} />
      <Route path="/checkout" element={<Checkout />} />
      <Route path="/confirm/:order" element={<Confirm/>} />
      <Route path="/single/:id" element={<Single/>} />
      <Route path="/register/:parrain_id?" element={<Register/>} />
      <Route path="/login" element={<Login/>} />
      <Route path="/profile" element={<Profile/>} />
    </Routes>
    <ToastContainer/>
    </>

  );
}

export default App;
