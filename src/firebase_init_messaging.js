/* eslint-disable no-undef */
import { initializeApp } from "firebase/app";
import { getMessaging, getToken, onMessage } from "firebase/messaging";
const firebaseConfig = {
  apiKey: "AIzaSyCPE_u2xVFO-csG2LDe3oivyvImRmDgRBg",
  authDomain: "lbu-app.firebaseapp.com",
  projectId: "lbu-app",
  storageBucket: "lbu-app.appspot.com",
  messagingSenderId: "926708914216",
  appId: "1:926708914216:web:62e97ba379fc13ac393d1d",
  measurementId: "G-14BM77M60G"
};

function requestPermission() {
  console.log('Requesting permission...');
  Notification.requestPermission().then((permission) => {
    if (permission === 'granted') {
      console.log('Notification permission granted.');
      // Initialize Firebase
      const app = initializeApp(firebaseConfig);


      // Initialize Firebase Cloud Messaging and get a reference to the service
      const messaging = getMessaging(app);
      onMessage(messaging, (payload) => {
        console.log('Message received. ', payload);
        // ...
      });
      // Add the public key generated from the console here.
      getToken(messaging, {vapidKey: "BOfirSaX1R8slShD91dx1_s_EmaoMBAV8kFZp-J6e5anuyb3Tsq1Yx1Vjr8LysGeWtBIDBpjxFhNDXEWe0V_Mps"}).then((currentToken) => {
          if (currentToken) {
            console.log('currentToken: ', currentToken);
          } else {
            // Show permission request UI
            console.log('No registration token available. Request permission to generate one.');
            // ...
          }
        }).catch((err) => {
          console.log('An error occurred while retrieving token. ', err);
          // ...
        });
    }else{
      console.log('Do not have permission')
    }

    
  });
}

  requestPermission();

