import React, {useEffect, useState} from 'react';
import { Link, redirect } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { validateToken } from '../config/Helper';

const HeaderShop = () => {
	const state = useSelector((state) => state.root.cart);
	const [auth, setAuth] = useState()
	useEffect(() => {
		validateToken().then(auth => {
			setAuth(auth)
			// if(!auth) {
			// 	return redirect("/home")
			// }
		})
	}, [])
    return ( 
        <>
            <div className="notice-top-bar bg-primary" data-sticky-start-at="180">
				<button className="hamburguer-btn hamburguer-btn-light notice-top-bar-close m-0 active" data-set-active="false">
					<span className="close">
						<span></span>
						<span></span>
					</span>
				</button>
				<div className="container">
					<div className="row justify-content-center py-2">
						<div className="col-9 col-md-12 text-center">
							<p className="text-color-light font-weight-semibold mb-0">Get Up to <strong>40% OFF</strong> New-Season Styles <a href="#" className="btn btn-primary-scale-2 btn-modern btn-px-2 btn-py-1 ms-2">MEN</a> <a href="#" className="btn btn-primary-scale-2 btn-modern btn-px-2 btn-py-1 ms-1 me-2">WOMAN</a> <span className="opacity-6 text-1">* Limited time only.</span></p>
						</div>
					</div>
				</div>
			</div>
			<header id="header" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': false, 'stickyStartAt': 135, 'stickySetTop': '-135px', 'stickyChangeLogo': true}">
				<div className="header-body header-body-bottom-border-fixed box-shadow-none border-top-0">
					<div className="header-top header-top-small-minheight header-top-simple-border-bottom">
						<div className="container">
							<div className="header-row justify-content-between">
								<div className="header-column col-auto px-0">
									<div className="header-row">
										<p className="font-weight-semibold text-1 mb-0 d-none d-sm-block d-md-none">FREE SHIPPING ORDERS $99+</p>
										<p className="font-weight-semibold text-1 mb-0 d-none d-md-block">FREE RETURNS, STANDARD SHIPPING ORDERS $99+</p>
									</div>
								</div>
								<div className="header-column justify-content-end col-auto px-0">
									<div className="header-row">
										<nav className="header-nav-top">
											<ul className="nav nav-pills font-weight-semibold text-2">
												<li className="nav-item dropdown nav-item-left-border d-lg-none">
													<a className="nav-link text-color-default text-color-hover-primary" href="#" role="button" id="dropdownMobileMore" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
														More
														<i className="fas fa-angle-down"></i>
													</a>
													<div className="dropdown-menu" aria-labelledby="dropdownMobileMore">
														<a className="dropdown-item" href="#">About</a>
														<a className="dropdown-item" href="#">Our Stores</a>
														<a className="dropdown-item" href="#">Blog</a>
														<a className="dropdown-item" href="#">Contact</a>
														<a className="dropdown-item" href="#">Help & FAQs</a>
														<a className="dropdown-item" href="#">Track Order</a>
													</div>
												</li>
												<li className="nav-item d-none d-lg-inline-block">
													<a href="#" className="text-decoration-none text-color-default text-color-hover-primary">About</a>
												</li>
												<li className="nav-item d-none d-lg-inline-block">
													<a href="#" className="text-decoration-none text-color-default text-color-hover-primary">Our Stores</a>
												</li>
												<li className="nav-item d-none d-lg-inline-block">
													<a href="#" className="text-decoration-none text-color-default text-color-hover-primary">Blog</a>
												</li>
												<li className="nav-item d-none d-lg-inline-block">
													<a href="#" className="text-decoration-none text-color-default text-color-hover-primary">Contact</a>
												</li>
												<li className="nav-item d-none d-xl-inline-block">
													<a href="#" className="text-decoration-none text-color-default text-color-hover-primary">Help & FAQs</a>
												</li>
												<li className="nav-item d-none d-xl-inline-block">
													<a href="#" className="text-decoration-none text-color-default text-color-hover-primary">Track Order</a>
												</li>
												<li className="nav-item dropdown nav-item-left-border">
													<a className="nav-link text-color-default text-color-hover-primary" href="#" role="button" id="dropdownCurrency" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
														USD
														<i className="fas fa-angle-down"></i>
													</a>
													<div className="dropdown-menu dropdown-menu-arrow-centered min-width-0" aria-labelledby="dropdownCurrency">
														<a className="dropdown-item" href="#">EUR</a>
														<a className="dropdown-item" href="#">USD</a>
													</div>
												</li>
												<li className="nav-item dropdown nav-item-right-border">
													<a className="nav-link text-color-default text-color-hover-primary" href="#" role="button" id="dropdownLanguage" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
														ENG
														<i className="fas fa-angle-down"></i>
													</a>
													<div className="dropdown-menu dropdown-menu-arrow-centered min-width-0" aria-labelledby="dropdownLanguage">
														<a className="dropdown-item" href="#">ESP</a>
														<a className="dropdown-item" href="#">FRA</a>
														<a className="dropdown-item" href="#">ENG</a>
													</div>
												</li>
											</ul>
											<ul className="header-social-icons social-icons social-icons-clean social-icons-icon-gray">
												<li className="social-icons-facebook">
													<a href="http://www.facebook.com/" target="_blank" title="Facebook"><i className="fab fa-facebook-f"></i></a>
												</li>
												<li className="social-icons-twitter">
													<a href="http://www.twitter.com/" target="_blank" title="Twitter"><i className="fab fa-twitter"></i></a>
												</li>
												<li className="social-icons-linkedin">
													<a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i className="fab fa-linkedin-in"></i></a>
												</li>
											</ul>
										</nav>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div className="header-container container">
						<div className="header-row py-2">
							<div className="header-column w-100">
								<div className="header-row justify-content-between">
									<div className="header-logo z-index-2 col-lg-2 px-0">
										<a href="index.html">
										<img alt="LBU" width="70" height="70" data-sticky-width="82" data-sticky-height="60" data-sticky-top="25" src="assets/img/logolbu.png"/>

										</a>
									</div>
									<div className="header-nav-features header-nav-features-no-border col-lg-5 col-xl-6 px-0 ms-0">
										<div className="header-nav-feature ps-lg-5 pe-lg-4">
											<form role="search" action="page-search-results.html" method="get">
												<div className="search-with-select">
													<a href="#" className="mobile-search-toggle-btn me-2" data-porto-toggle-className="open">
														<i className="icons icon-magnifier text-color-dark text-color-hover-primary"></i>
													</a>
													<div className="search-form-wrapper input-group">
														<input className="form-control text-1" id="headerSearch" name="q" type="search" value="" placeholder="Search..."/>
														<div className="search-form-select-wrapper">
															<div className="custom-select-1">
																<select name="category" className="form-control form-select">
																	<option value="all" selected>All Categories</option>
																	<option value="fashion">Fashion</option>
																	<option value="electronics">Electronics</option>
																	<option value="homegarden">Home & Garden</option>
																	<option value="motors">Motors</option>
																	<option value="features">Features</option>
																</select>
															</div>
															<button className="btn" type="submit">
																<i className="icons icon-magnifier header-nav-top-icon text-color-dark"></i>
															</button>
														</div>
													</div>
												</div>
											</form>
										</div>
									</div>
									<ul className="header-extra-info col-lg-3 col-xl-2 ps-2 ps-xl-0 ms-lg-3 d-none d-lg-block">
										<li className="d-none d-sm-inline-flex ms-0">
											<div className="header-extra-info-icon ms-lg-4">
												<i className="icons icon-phone text-3 text-color-dark position-relative top-1"></i>
											</div>
											<div className="header-extra-info-text">
												<label className="text-1 font-weight-semibold text-color-default">CALL US NOW</label>
												<strong className="text-4"><a href="tel:+1234567890" className="text-color-hover-primary text-decoration-none">+123 4567 890</a></strong>
											</div>
										</li>
									</ul>
									<div className="d-flex col-auto col-lg-2 pe-0 ps-0 ps-xl-3">
										<ul className="header-extra-info">
											<li className="ms-0 ms-xl-4">
												<div className="header-extra-info-icon">
													{!auth ? 
													<Link to="/register" className="text-decoration-none text-color-dark text-color-hover-primary text-2">
														<i className="icons icon-user"></i>
													</Link> : 
													<Link to="/profile" className="text-decoration-none text-color-dark text-color-hover-primary text-2">
														<i className="icons icon-user"></i>
													</Link> }
												</div>
											</li>
											<li className="me-2 ms-3">
												<div className="header-extra-info-icon">
													<a href="#" className="text-decoration-none text-color-dark text-color-hover-primary text-2">
														<i className="icons icon-heart"></i>
													</a>
												</div>
											</li>
										</ul>
										<div className="header-nav-features ps-0 ms-1">
											<div className="header-nav-feature header-nav-features-cart header-nav-features-cart-big d-inline-flex top-2 ms-2">
												<Link to="/cart" className="header-nav-features-toggle">
													<img src="assets/img/icons/icon-cart-big.svg" height="30" alt="" className="header-nav-top-icon-img"/>
													<span className="cart-info">
														<span className="cart-qty">{state && state.length}</span>
													</span>
												</Link>
												<div className="header-nav-features-dropdown" id="headerTopCartDropdown">
													<ol className="mini-products-list">
														<li className="item">
															<a href="#" title="Camera X1000" className="product-image"><img src="assets/img/products/product-1.jpg" alt="Camera X1000"/></a>
															<div className="product-details">
																<p className="product-name">
																	<a href="#">Camera X1000 </a>
																</p>
																<p className="qty-price">
																	 1X <span className="price">$890</span>
																</p>
																<a href="#" title="Remove This Item" className="btn-remove"><i className="fas fa-times"></i></a>
															</div>
														</li>
													</ol>
													<div className="totals">
														<span className="label">Total:</span>
														<span className="price-total"><span className="price">$890</span></span>
													</div>
													<div className="actions">
														<a className="btn btn-dark" href="#">View Cart</a>
														<a className="btn btn-primary" href="#">Checkout</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div className="header-column justify-content-end">
								<div className="header-row">

								</div>
							</div>
						</div>
					</div>
					<div className="header-nav-bar header-nav-bar-top-border bg-light">
						<div className="header-container container">
							<div className="header-row">
								<div className="header-column">
									<div className="header-row justify-content-end">
										<div className="header-nav header-nav-line header-nav-top-line header-nav-top-line-with-border justify-content-start" data-sticky-header-style="{'minResolution': 991}" data-sticky-header-style-active="{'margin-left': '105px'}" data-sticky-header-style-deactive="{'margin-left': '0'}">
											<div className="header-nav-main header-nav-main-square header-nav-main-dropdown-no-borders header-nav-main-effect-3 header-nav-main-sub-effect-1 w-100">
												<nav className="collapse w-100">
													<ul className="nav nav-pills w-100" id="mainNav">
														<li className="dropdown">
															<Link className="dropdown-item dropdown-toggle" to="/">
																Accueil
                                                            </Link>
                                                        </li>
                                                        <li className="dropdown">
															<Link className="dropdown-item dropdown-toggle active" to="/shop">
																Boutique
                                                            </Link>
                                                        </li>
                                                        <li className="dropdown">
															<a className="dropdown-item dropdown-toggle" href="index.html">
																Contact
                                                            </a>
                                                        </li>
													</ul>
												</nav>
											</div>
											<button className="btn header-btn-collapse-nav" data-bs-toggle="collapse" data-bs-target=".header-nav-main nav">
												<i className="fas fa-bars"></i>
											</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>
        </>
     );
}
 
export default HeaderShop;