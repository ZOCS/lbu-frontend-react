import React from 'react'

export default function Footer() {
  return (
    <footer id="footer">
				<div className="container">
					<div className="footer-ribbon">
						<span>Librairie LBU</span>
					</div>
					<div className="row py-5 my-4">
						<div className="col-md-6 col-lg-4 mb-4 mb-lg-0">
							<h5 className="text-3 mb-3">NEWSLETTER</h5>
							<p className="pe-1">Abonnez-vous à notre newsletter et ne rattez plus aucune offre et avantage LBU</p>
							<div className="alert alert-success d-none" id="newsletterSuccess">
								<strong>Success!</strong> You've been added to our email list.
							</div>
							<div className="alert alert-danger d-none" id="newsletterError"></div>
							<form id="newsletterForm" action="php/newsletter-subscribe.php" method="POST" className="me-4 mb-3 mb-md-0">
								<div className="input-group input-group-rounded">
									<input className="form-control form-control-sm bg-light" placeholder="Entrez votre adresse mail" name="newsletterEmail" id="newsletterEmail" type="email"/>
									<button className="btn btn-light text-color-dark" type="submit"><strong>Souscrire</strong></button>
								</div>
							</form>
						</div>
						<div className="col-md-6 col-lg-3 mb-4 mb-lg-0">
							<h5 className="text-3 mb-3">MON COMPTE</h5>
							<div id="tweet" className="twitter" data-plugin-tweets data-plugin-options="{'username': 'oklerthemes', 'count': 2}">
								<p>Mon commandes</p>
								<p>Mes wishlist</p>
							</div>
						</div>
						<div className="col-md-6 col-lg-3 mb-4 mb-md-0">
							<div className="contact-details">
								<h5 className="text-3 mb-3">CONTACTEZ-NOUS</h5>
								<ul className="list list-icons list-icons-lg">
									<li className="mb-1"><i className="far fa-dot-circle text-color-primary"></i><p className="m-0">Rue GANHI, N° 508 Avenue Clozel. En face du marché Ganhi. Cotonou - BENIN</p></li>
									<li className="mb-1"><i className="fab fa-whatsapp text-color-primary"></i><p className="m-0"><a href="tel:8001234567">(+229) 65 553 202</a></p></li>
									<li className="mb-1"><i className="far fa-envelope text-color-primary"></i><p className="m-0"><a href="mailto:mail@example.com">librairielbu@librairielbu.com</a></p></li>
									<li className="mb-1"><i className="far fa-clock text-color-primary"></i><p className="m-0"><a href="mailto:mail@example.com">Lun - Ven 8:00 - 19:00 / Sam 9:00 - 19:00</a></p></li>
								</ul>
							</div>
						</div>
						<div className="col-md-6 col-lg-2">
							<h5 className="text-3 mb-3">FOLLOW US</h5>
							<ul className="social-icons">
								<li className="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i className="fab fa-facebook-f"></i></a></li>
								<li className="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i className="fab fa-twitter"></i></a></li>
								<li className="social-icons-linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i className="fab fa-linkedin-in"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
				<div className="footer-copyright">
					<div className="container py-2">
						<div className="row py-4">
							<div className="col-lg-1 d-flex align-items-center justify-content-center justify-content-lg-start mb-2 mb-lg-0">
								<a href="index.html" className="logo pe-0 pe-lg-3">
									<img alt="Porto Website Template" src="assets/img/logo-footer.png" className="opacity-5" width="49" height="22" data-plugin-options="{'appearEffect': 'fadeIn'}"/>
								</a>
							</div>
							<div className="col-lg-7 d-flex align-items-center justify-content-center justify-content-lg-start mb-4 mb-lg-0">
								<p>© Group GMD. 2018. Tous droits réservés</p>
							</div>
							<div className="col-lg-4 d-flex align-items-center justify-content-center justify-content-lg-end">
								<nav id="sub-menu">
									<ul>
										<li><i className="fas fa-angle-right"></i><a href="page-faq.html" className="ms-1 text-decoration-none"> FAQ's</a></li>
										<li><i className="fas fa-angle-right"></i><a href="sitemap.html" className="ms-1 text-decoration-none"> Sitemap</a></li>
										<li><i className="fas fa-angle-right"></i><a href="contact-us.html" className="ms-1 text-decoration-none"> Contact Us</a></li>
									</ul>
								</nav>
							</div>
						</div>
					</div>
				</div>
			</footer>
  )
}
