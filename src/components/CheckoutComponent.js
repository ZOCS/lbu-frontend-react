/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";
import { getResource, postResource } from "../config/Api";
import {
	openKkiapayWidget,
	addKkiapayListener,
	removeKkiapayListener,
  } from "kkiapay";
  import { useSelector, useDispatch } from 'react-redux';
import { onServerError, onServerSuccess } from "../config/Helper";
import axios from "axios";
import { resetCart } from "../store/action/action";

const CheckoutComponent = () => {
	const navigate = useNavigate();
	const [succeess , setSuccess] = useState(false)
	const [transactionId , setTransactionId] = useState('')
	const [selectedValue, setSelectedValue] = useState('');
	const [methode, setMethode] = useState([])
	const [data, setData] = useState({
		name: '',
		email: '',
		phone: '',
		adresse:'',
		zip : '',
		pays: '',
		ville: '',
		payement_id: '',
		method_payement: 'kkiapay',
		code: '',
		prix: '',	
	})
	const _init_ = () => {
		getResource('type_paiements').then((res) => {
			setMethode(res.data.data)
		})
	}

		//Recuperation des produits du reducer
		const cart = useSelector((state) => state.root.cart);

	const dispatch = useDispatch();
	// Fonction pour faire le calcul total du panier
	const getTotalAmount = () =>{
		let amount = 0;
		cart.map((c) => {
			// let itemInfo = cart.find((p) => p.id === Number(i));
				amount += c.qty * c.price_regular
		})
				
		return amount;
	}

	const handleRadioChange = (event) => {
    setSelectedValue(event.target.value);
  };

	const handleInput = (e) => {
		const { name, value } = e.target;
	
		setData((s) => ({
		  ...s,
		  [name]: value,
		}));
	  };
	

	const handleOrder = () => {
		// const id = "02af002b-b213-44cf-afdf-95d581e2d770";
		// getResource(`/generate-pdf/${id}`).then(res => {
		// 	// console.log(res.data)
		// }).catch(e => {
		// 	console.log(e.response)
		// })
		// let newCart = cart.map(({ qty, ...product }) => ({ ...product, quantity: qty }));
		//   let newData = {...data, products: newCart};
		//   console.log(newData);
		openKkiapayWidget({
			amount: getTotalAmount(),
			api_key: "ea65eca024de11ebb45c4706c718fb6d",
			sandbox: true,
			email: "zossourougail@gmail.com",
			telephone: "97000000",
		  });
		  addKkiapayListener('success',successHandler)
		  
	}
	 const postOrder = (response) => {
		let newCart = cart.map(({ qty, ...product }) => ({ ...product, quantity: qty }));
		  let newData = {...data, products: newCart, payement_id: response, prix: getTotalAmount(), pays: "Benin"};
		  
		  console.log(newData);
		postResource('/orders', newData).then(res => {
			console.log(res.data, res.status)
			if(res.status === 201){
				const info = res.data.order
				console.log(info.id)
				printInvoice(info.id, info.code)
				dispatch(resetCart())
				onServerSuccess("Votre commande est bien envoye")
				navigate(`/confirm/${info.id}`);
			}
		}).catch(error => {
			if(error.response.status === 500) {
				onServerError("Erreur de Serveur")
			}else if(error.response.status === 500) {
				onServerError(error.message)
			}
		})
	 }

	 const printInvoice = (id, name) => {
        axios.get(`https://api-librairielbu.apaid-benin.org/api/generate-pdf/${id}`, {headers: {
            Accept: 'aaplication/pdf',
            "Access-Control-Allow-Origin": "*",
            "X-Requested-With": "XMLHttpRequest",
            xsrfHeaderName: "X-XSRF-TOKEN",
            withCredentials: true,
        },
        responseType: 'blob'
    }).then(res => {
            console.log(res.data)
            // const pdf = res.data.blob();
            // const downloadLink = document.createElement('a');
            // downloadLink.href = URL.createObjectURL(pdf);
            // downloadLink.download = 'carte.pdf';
			const contentDisposition = res.headers['content-disposition'];
			const matches = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/.exec(contentDisposition);
  			let filename = '';

            const blob = new Blob([res.data], { type: 'application/pdf' });
            const url = window.URL.createObjectURL(blob);
            const a = document.createElement('a');
            a.href = url;
            if (matches != null && matches[1]) {
				filename = matches[1].replace(/['"]/g, ''); // Nettoie le nom du fichier
			  } else {
				// Si le nom de fichier n'est pas trouvé dans le header, générez un nom par défaut
				filename = `invoice_${name}.pdf`;
			  }
            a.setAttribute('download', filename);
            document.body.appendChild(a);
            a.click();
            window.URL.revokeObjectURL(url);
        }).catch(e => {
            onServerError(e.response)
        })
	}


	function successHandler(response) {
		console.log(response);
		setTransactionId(response.transactionId)
		postOrder(response.transactionId)
		
	  }

	useEffect(() => {
		console.log(cart)
		// addKkiapayListener('success',successHandler)
		// return () => {
		//   removeKkiapayListener('success',successHandler)
		// };
	  }, []);
    return ( 
        <div className="container">

					<div className="row">
						<div className="col">
							<ul className="breadcrumb breadcrumb-dividers-no-opacity font-weight-bold text-6 justify-content-center my-5">
								<li className="text-transform-none me-2">
									<Link to="/cart" className="text-decoration-none text-color-dark text-color-hover-primary">Panier</Link>
								</li>
								<li className="text-transform-none text-color-dark me-2">
									<Link to="/checkout" className="text-decoration-none text-color-primary">Caisse</Link>
								</li>
								<li className="text-transform-none text-color-grey-lighten">
									<Link to="/confirm" className="text-decoration-none text-color-grey-lighten text-color-hover-primary">Commande terminée</Link>
								</li>
							</ul>
						</div>
					</div>

					<div className="row">
						<div className="col">
							<p className="mb-1">Déjà client?<a href="#" className="text-color-dark text-color-hover-primary text-uppercase text-decoration-none font-weight-bold" data-bs-toggle="collapse" data-bs-target=".login-form-wrapper">Se connecter</a></p>
						</div>
					</div>

					<div className="row login-form-wrapper collapse mb-5">
						<div className="col">
							<div className="card border-width-3 border-radius-0 border-color-hover-dark">
								<div className="card-body">
									<form action="/" id="frmSignIn" method="post">
										<div className="row">
											<div className="form-group col">
												<label className="form-label text-color-dark text-3">Email address <span className="text-color-danger">*</span></label>
												<input type="text" value="" className="form-control form-control-lg text-4" required/>
											</div>
										</div>
										<div className="row">
											<div className="form-group col">
												<label className="form-label text-color-dark text-3">Password <span className="text-color-danger">*</span></label>
												<input type="password" value="" className="form-control form-control-lg text-4" required/>
											</div>
										</div>
										<div className="row justify-content-between">
											<div className="form-group col-md-auto">
												<div className="custom-control custom-checkbox">
													<input type="checkbox" className="custom-control-input" id="rememberme"/>
													<label className="form-label custom-control-label cur-pointer text-2" for="rememberme">Remember Me</label>
												</div>
											</div>
											<div className="form-group col-md-auto">
												<a className="text-decoration-none text-color-dark text-color-hover-primary font-weight-semibold text-2" href="#">Mot de passe oublié?</a>
											</div>
										</div>
										<div className="row">
											<div className="form-group col">
												<button type="submit" className="btn btn-dark btn-modern w-100 text-uppercase rounded-0 font-weight-bold text-3 py-3" data-loading-text="Loading...">Se connecter</button>
												<div className="divider">
													<span className="bg-light px-4 position-absolute left-50pct top-50pct transform3dxy-n50">or</span>
												</div>
												<a href="#" className="btn btn-primary-scale-2 btn-modern w-100 text-transform-none rounded-0 font-weight-bold align-items-center d-inline-flex justify-content-center text-3 py-3" data-loading-text="Loading..."><i className="fab fa-facebook text-5 me-2"></i> Login With Facebook</a>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>

					<div className="row">
						<div className="col">
							<p>Avez-vous un coupon?<a href="#" className="text-color-dark text-color-hover-primary text-uppercase text-decoration-none font-weight-bold" data-bs-toggle="collapse" data-bs-target=".coupon-form-wrapper">Entrez votre code</a></p>
						</div>
					</div>

					<div className="row coupon-form-wrapper collapse mb-5">
						<div className="col">
							<div className="card border-width-3 border-radius-0 border-color-hover-dark">
								<div className="card-body">
									<form role="form" method="post" action="">
										<div className="d-flex align-items-center">
											<input type="text" className="form-control h-auto border-radius-0 line-height-1 py-3" name="couponCode" placeholder="Coupon Code" required />
											<button type="submit" className="btn btn-light btn-modern text-color-dark bg-color-light-scale-2 text-color-hover-light bg-color-hover-primary text-uppercase text-3 font-weight-bold border-0 border-radius-0 ws-nowrap btn-px-4 py-3 ms-2">Apply Coupon</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>

					<form >
						<div className="row">
							<div className="col-lg-7 mb-4 mb-lg-0">

								<h2 className="text-color-dark font-weight-bold text-5-5 mb-3">Détails de la facturation</h2>
								<div className="row">
									<div className="form-group col">
										<label className="form-label">Nom et Prénom(s) <span className="text-color-danger">*</span></label>
										<input type="text" className="form-control h-auto py-2" onChange={handleInput} name="name" value={data.name} required />
									</div>
								</div>
								<div className="row">
									<div className="form-group col">
										<label className="form-label">Nom de l'entreprise</label>
										<input type="text" className="form-control h-auto py-2" name="companyName" value="" />
									</div>
								</div>
								<div className="row">
									<div className="form-group col">
										<label className="form-label">Pays <span className="text-color-danger">*</span></label>
										<div className="custom-select-1">
											<select className="form-select form-control h-auto py-2" name="pays" value={data.pays} onChange={handleInput} required>
												<option value="" selected></option>
												<option value="usa">United States</option>
												<option value="spa">Spain</option>
												<option value="fra">France</option>
												<option value="uk">United Kingdon</option>
											</select>
										</div>
									</div>
								</div>
								<div className="row">
									<div className="form-group col">
										<label className="form-label">Adresse de residence <span className="text-color-danger">*</span></label>
										<input type="text" className="form-control h-auto py-2" name="adresse" onChange={handleInput} value={data.adresse} placeholder="House number and street name" required />
									</div>
								</div>
								<div className="row">
									<div className="form-group col">
										<label className="form-label">Ville <span className="text-color-danger">*</span></label>
										<input type="text" className="form-control h-auto py-2" name="ville" value={data.ville} onChange={handleInput} required />
									</div>
								</div>
								<div className="row">
									<div className="form-group col">
										<label className="form-label">ZIP <span className="text-color-danger">*</span></label>
										<input type="text" onChange={handleInput} className="form-control h-auto py-2" name="zip" value={data.zip} required />
									</div>
								</div>
								<div className="row">
									<div className="form-group col">
										<label className="form-label">Téléphone <span className="text-color-danger">*</span></label>
										<input type="number" onChange={handleInput} className="form-control h-auto py-2" name="phone" value={data.phone} required />
									</div>
								</div>
								<div className="row">
									<div className="form-group col">
										<label className="form-label">Adresse Email <span className="text-color-danger">*</span></label>
										<input type="email" className="form-control h-auto py-2" onChange={handleInput} name="email" value={data.email} required />
									</div>
								</div>
								<div className="row">
									<div className="form-group col">
										<div className="custom-checkbox-1">
											<input id="createAccount" type="checkbox" name="createAccount" value="1" />
											<label for="createAccount">Create an account ?</label>
										</div>
									</div>
								</div>
								<div className="row">
									<div className="form-group col">
										<div className="custom-checkbox-1" data-bs-toggle="collapse" data-bs-target=".shipping-field-wrapper">
											<input id="shipAddress" type="checkbox" name="shipAddress" value="1" />
											<label for="shipAddress">Magasinez à une autre adresse ?</label>
										</div>
									</div>
								</div>
								/*-- Ship to a differente address fields --*/
								<div className="shipping-field-wrapper collapse">
									<div className="row">
										<div className="form-group col-md-6">
											<label className="form-label">First Name <span className="text-color-danger">*</span></label>
											<input type="text" className="form-control h-auto py-2" name="shippingFirstName" value="" required />
										</div>
										<div className="form-group col-md-6">
											<label className="form-label">Last Name <span className="text-color-danger">*</span></label>
											<input type="text" className="form-control h-auto py-2" name="shippingLastName" value="" required />
										</div>
									</div>
									<div className="row">
										<div className="form-group col">
											<label className="form-label">Company Name</label>
											<input type="text" className="form-control h-auto py-2" name="shippingCompanyName" value="" />
										</div>
									</div>
									<div className="row">
										<div className="form-group col">
											<label className="form-label">Country/Region <span className="text-color-danger">*</span></label>
											<div className="custom-select-1">
												<select className="form-select form-control h-auto py-2" name="shippingCountry" required>
													<option value="" selected></option>
													<option value="usa">United States</option>
													<option value="spa">Spain</option>
													<option value="fra">France</option>
													<option value="uk">United Kingdon</option>
												</select>
											</div>
										</div>
									</div>
									<div className="row">
										<div className="form-group col">
											<label className="form-label">Street Address <span className="text-color-danger">*</span></label>
											<input type="text" className="form-control h-auto py-2" name="shippingAddress1" value="" placeholder="House number and street name" required />
										</div>
									</div>
									<div className="row">
										<div className="form-group col">
											<input type="text" className="form-control h-auto py-2" name="shippingAddress2" value="" placeholder="Apartment, suite, unit, etc..." required/>
										</div>
									</div>
									<div className="row">
										<div className="form-group col">
											<label className="form-label">Town/City <span className="text-color-danger">*</span></label>
											<input type="text" className="form-control h-auto py-2" name="shippingCity" value="" required />
										</div>
									</div>
									<div className="row">
										<div className="form-group col">
											<label className="form-label">State <span className="text-color-danger">*</span></label>
											<div className="custom-select-1">
												<select className="form-select form-control h-auto py-2" name="shippingState" required>
													<option value="" selected></option>
													<option value="ny">Nova York</option>
													<option value="ca">California</option>
													<option value="tx">Texas</option>
													<option value="">Florida</option>
												</select>
											</div>
										</div>
									</div>
									<div className="row">
										<div className="form-group col">
											<label className="form-label">ZIP <span className="text-color-danger">*</span></label>
											<input type="text" className="form-control h-auto py-2" name="shippingZip" value="" required />
										</div>
									</div>
									<div className="row">
										<div className="form-group col">
											<label className="form-label">Phone <span className="text-color-danger">*</span></label>
											<input type="number" className="form-control h-auto py-2" name="shippingPhone" value="" required />
										</div>
									</div>
									{/* /* End of Ship to a differente address fields */}
								</div>
								<div className="row">
									<div className="form-group col">
										<label className="form-label">Order Notes</label>
										<textarea className="form-control h-auto py-2" name="orderNotes" rows="5" placeholder="Notes about you orderm e.g. special notes for delivery"></textarea>
									</div>
								</div>

							</div>
							<div className="col-lg-5 position-relative">
								<div className="card border-width-3 border-radius-0 border-color-hover-dark" data-plugin-sticky data-plugin-options="{'minWidth': 991, 'containerSelector': '.row', 'padding': {'top': 85}}">
									<div className="card-body">
										<h4 className="font-weight-bold text-uppercase text-4 mb-3">Votre commande</h4>
										<table className="shop_table cart-totals mb-3">
											<tbody>
												<tr>
													<td colspan="2" className="border-top-0">
														<strong className="text-color-dark">Produits</strong>
													</td>
												</tr>
												
													{cart.slice(0, 2).map(x => (
														<tr>
													<td>
														<strong className="d-block text-color-dark line-height-1 font-weight-semibold">{x.name}<span className="product-qty">{` x${x.qty}`}</span></strong>
														<span className="text-1">{x.categories.map(x => x.name)}</span>
													</td>
													<td className="text-end align-top">
														<span className="amount font-weight-medium text-color-grey">{`${x.price_regular} XOF`}</span>
													</td>
													</tr>))}
												<tr className="shipping">
													<td colspan="2">
														<strong className="d-block text-color-dark mb-2">Livraison</strong>

														<div className="d-flex flex-column">
															<label className="d-flex align-items-center text-color-grey mb-0" for="shipping_method1">
																<input id="shipping_method1" type="radio" className="me-2" name="shipping_method" value="free" checked />
																Free Shipping
															</label>
															<label className="d-flex align-items-center text-color-grey mb-0" for="shipping_method2">
																<input id="shipping_method2" type="radio" className="me-2" name="shipping_method" value="local-pickup" />
																Local Pickup
															</label>
															<label className="d-flex align-items-center text-color-grey mb-0" for="shipping_method3">
																<input id="shipping_method3" type="radio" className="me-2" name="shipping_method" value="flat-rate" />
																Flat Rate: $5.00
															</label>
														</div>
													</td>
												</tr>
												<tr className="total">
													<td>
														<strong className="text-color-dark text-3-5">Total</strong>
													</td>
													<td className="text-end">
														<strong className="text-color-dark"><span className="amount text-color-dark text-5">{`${getTotalAmount()} XOF`}</span></strong>
													</td>
												</tr>
												<tr className="payment-methods">
													<td colspan="2">
														<strong className="d-block text-color-dark mb-2">Mode de livraison</strong>

														<div className="d-flex flex-column">
														Livraison : Au point de vente LBU	
														</div>
													</td>
												</tr>
												<tr className="payment-methods">
													<td colspan="2">
														<strong className="d-block text-color-dark mb-2">Methode de paiement</strong>

														{methode.map(t => <div className="d-flex flex-column">
															<label className="d-flex align-items-center text-color-grey mb-0" for="Kkiapay">
																<input id="Kkiapay" type="radio" value="Kkiapay" checked={selectedValue === 'Kkiapay'} onChange={handleRadioChange} className="me-2" name="payment_method"/>
																{t.libelle}
																
															</label>
															{selectedValue === "Kkiapay" && <div className="d-flex m-2">
																<img alt="MTN Momo" style={{  marginRight: 10}} width="50" height="50" data-sticky-width="82" data-sticky-height="60" data-sticky-top="25" src="assets/img/mtn.png"/>
																<img alt="Flooz" width="30" height="40" data-sticky-width="82" data-sticky-height="60" data-sticky-top="25" src="assets/img/flooz.png"/>
															</div>}
															<label className="d-flex align-items-center text-color-grey mb-0" for="payment_method2">
																<input id="payment_method2" type="radio" className="me-2" name="Virement"  value="Virement" checked={selectedValue === 'Virement'} onChange={handleRadioChange} />
																Virement bancaire
															</label>
															{selectedValue === "Virement" && <p>Effectuez votre règlement directement sur notre compte bancaire (n’oubliez pas de mentionnez le numéro de commande). Votre commande ne sera pas expédiée tant que votre règlement n’aura pas été crédité à notre compte.</p>}
														</div>)}
													</td>
												</tr>
												<tr>
													<td colspan="2">
													Vos données personnelles seront utilisées pour traiter votre commande, soutenir votre expérience sur ce site Web et à d'autres fins décrites dans notre politique de confidentialité.
													</td>
												</tr>
											</tbody>
										</table>
										<button type="button" onClick={handleOrder} className="btn btn-dark btn-modern w-100 text-uppercase bg-color-hover-primary border-color-hover-primary border-radius-0 text-3 py-3">Place Order <i className="fas fa-arrow-right ms-2"></i></button>
									</div>
								</div>
							</div>
						</div>
					</form>

				</div>
     );
}
 
export default CheckoutComponent;