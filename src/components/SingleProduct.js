import React, {useEffect, useState} from 'react';
import { Link, useParams } from "react-router-dom";
import { getResource } from '../config/Api';
import { onServerError, onServerSuccess } from '../config/Helper';
import AppConfig from '../config/AppConfig';
import Loading from './Loading';
import { addCart } from '../store/action/action';
import { useDispatch } from 'react-redux';

const SingleProduct = () => {
	const [product, setProduct] = useState({})
	const [loading, setLoading] = useState(true)
	const [products, setProducts] = useState([])
	const [featured, setFeatured] = useState([])
	const [promo, setPromo] = useState([])
	const [popular, setPopular] = useState([])
	const [newArrival, setNewArrival] = useState([])
	const [similar, setSimilar] = useState([])
	const {id} = useParams()
	const _init_ = async() => {
		await getResource(`products/${id}`).then((res) => {
			// console.log(res.data)
			setProduct(res.data['product']);
			setSimilar(res.data["similar_products"])
		}).catch(e => {
			if(e.response.status === 500){
				onServerError(e.response.message)
			}else if(e.response.status === 404){
				onServerError(e.response.message)
			}
		})

		await getResource("/products").then((res) => {
			console.log(res.data)
			const data = res.data["data"];
			

			const featuredProducts = [];
			const popularProducts = [];
			const newArrivalProducts = [];
			const promoProducts = [];

			data.forEach((product) => {
			if (product.featured === 1) {
				featuredProducts.push(product);
			}
			if (product.popular === 1) {
				popularProducts.push(product);
			}
			if (product.new_arrival === 1) {
				newArrivalProducts.push(product);
			}
			if (product.promo === 1) {
				promoProducts.push(product);
			}
			});

			setFeatured(featuredProducts);
			setPopular(popularProducts);
			setNewArrival(newArrivalProducts);
			setPromo(promoProducts);
		}).catch(e => {
			if(e.response.status === 500){
				onServerError(e.response.message)
			}else if(e.response.status === 404){
				onServerError(e.response.message)
			}
		})
		setLoading(false);
	}

	const dispatch = useDispatch()
	const addProduct = (product) => {
        // console.log(product)
        dispatch(addCart(product));
        onServerSuccess(`${product.name} ajouter au panier`)
    }


	useEffect(() => {
		_init_()
	},[])

	if (loading) {
		return (
		<>
			<Loading />
		</>
		);
  	}
    return ( 
        <div className="container">

					<div className="row">
						<div className="col">
							<ul className="breadcrumb breadcrumb-style-2 d-block text-4 mb-4">
								<li><a href="#" className="text-color-default text-color-hover-primary text-decoration-none">Home</a></li>
								<li><a href="#" className="text-color-default text-color-hover-primary text-decoration-none">Electronics</a></li>
								<li>SMARTWATCHES</li>
							</ul>
						</div>
					</div>
					<div className="row">
						<div className="col-md-5 mb-5 mb-md-0">

							<div className="thumb-gallery-wrapper">
								<div className="  nav-inside nav-style-1 nav-dark mb-3">
									<div>
										<img alt="" className="img-fluid" src={product.image && `${AppConfig.apiUrlWeb}/${product.image}`} data-zoom-image={product.image && `${AppConfig.apiUrlWeb}/${product.image}`}/>
									</div>
									{/* <div>
										<img alt="" className="img-fluid" src={product.image && `${AppConfig.apiUrlWeb}/${product.image}`} data-zoom-image={product.image && `${AppConfig.apiUrlWeb}/${product.image}`}/>
									</div>
									<div>
										<img alt="" className="img-fluid" src={product.image && `${AppConfig.apiUrlWeb}/${product.image}`} data-zoom-image={product.image && `${AppConfig.apiUrlWeb}/${product.image}`}/>
									</div>
									<div>
										<img alt="" className="img-fluid" src={product.image && `${AppConfig.apiUrlWeb}/${product.image}`} data-zoom-image={product.image && `${AppConfig.apiUrlWeb}/${product.image}`}/>
									</div>
									<div>
										<img alt="" className="img-fluid" src="assets/img/products/product-grey-7-5.jpg" data-zoom-image="assets/img/products/product-grey-7-5.jpg"/>
									</div> */}
								</div>
								{/* <div className="thumb-gallery-thumbs owl-carousel owl-theme manual thumb-gallery-thumbs">
									<div className="cur-pointer">
										<img alt="" className="img-fluid" src="assets/img/products/product-grey-7.jpg"/>
									</div>
									<div className="cur-pointer">
										<img alt="" className="img-fluid" src="assets/img/products/product-grey-7-2.jpg"/>
									</div>
									<div className="cur-pointer">
										<img alt="" className="img-fluid" src="assets/img/products/product-grey-7-3.jpg" />
									</div>
									<div className="cur-pointer">
										<img alt="" className="img-fluid" src="assets/img/products/product-grey-7-4.jpg"/>
									</div>
									<div className="cur-pointer">
										<img alt="" className="img-fluid" src="assets/img/products/product-grey-7-5.jpg"/>
									</div>
								</div> */}
							</div>

						</div>

						<div className="col-md-7">

							<div className="summary entry-summary position-relative">

								<div className="position-absolute top-0 right-0">
									<div className="products-navigation d-flex">
										<a href="#" className="prev text-decoration-none text-color-dark text-color-hover-primary border-color-hover-primary" data-bs-toggle="tooltip" data-bs-animation="false" data-bs-original-title="Red Ladies Handbag"><i className="fas fa-chevron-left"></i></a>
										<a href="#" className="next text-decoration-none text-color-dark text-color-hover-primary border-color-hover-primary" data-bs-toggle="tooltip" data-bs-animation="false" data-bs-original-title="Green Ladies Handbag"><i className="fas fa-chevron-right"></i></a>
									</div>
								</div>

								<h1 className="mb-0 font-weight-bold text-7">{product.name}</h1>

								<div className="pb-0 clearfix d-flex align-items-center">
									<div title="Rated 3 out of 5" className="float-start">
										<input type="text" className="d-none" value="3" title="" data-plugin-star-rating data-plugin-options="{'displayOnly': true, 'color': 'primary', 'size':'xs'}" />
									</div>

									<div className="review-num">
										<a href="#description" className="text-decoration-none text-color-default text-color-hover-primary" data-hash data-hash-offset="0" data-hash-offset-lg="75" data-hash-trigger-click=".nav-link-reviews" data-hash-trigger-click-delay="1000">
											<span className="count text-color-inherit" itemprop="ratingCount">(2</span> reviews)
										</a>
									</div>
								</div>

								<div className="divider divider-small">
									<hr className="bg-color-grey-scale-4"/>
								</div>

								<p className="price mb-3">
									<span className="sale text-color-dark">{product.price_regular} Fcfa</span>
									<span className="amount">{product.price_promo} Fcfa</span>
								</p>

								{/* <p className="text-3-5 mb-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus nibh sed elimttis adipiscing. Fusce in hendrerit purus. Lorem ipsum dolor sit amet.</p> */}

								<ul className="list list-unstyled text-2">
									<li className="mb-0">DISPONIBILITÉ: <strong className="text-color-dark">{product.status}</strong></li>
									<li className="mb-0">SKU: <strong className="text-color-dark">{product.isbn}</strong></li>
								</ul>

								<form>
									{/* <table className="table table-borderless" style={{ maxWidth: '300px' }}>
										<tbody> 
										<tr>
												<td className="align-middle text-2 px-0 py-2">SIZE:</td>
												<td className="px-0 py-2">
													<div className="custom-select-1">
														<select name="size" className="form-control form-select text-1 h-auto py-2">
															<option value="">PLEASE CHOOSE</option>
															<option value="blue">Small</option>
															<option value="red">Normal</option>
															<option value="green">Big</option>
														</select>
													</div>
												</td>
											</tr> 
											<tr>
												<td className="align-middle text-2 px-0 py-2">COLOR:</td>
												<td className="px-0 py-2">
													<div className="custom-select-1">
														<select name="color" className="form-control form-select text-1 h-auto py-2">
															<option value="">PLEASE CHOOSE</option>
															<option value="blue">Blue</option>
															<option value="red">Red</option>
															<option value="green">Green</option>
														</select>
													</div>
												</td>
											</tr>
										</tbody>
									</table>*/}
									<hr/>
									<div className="quantity quantity-lg">
										<input type="button" className="minus text-color-hover-light bg-color-hover-primary border-color-hover-primary" value="-" />
										<input type="text" className="input-text qty text" title="Qty" value="1" name="quantity" min="1" step="1" />
										<input type="button" className="plus text-color-hover-light bg-color-hover-primary border-color-hover-primary" value="+" />
									</div>
									<button type="submit" className="btn btn-dark btn-modern text-uppercase bg-color-hover-primary border-color-hover-primary">Add to cart</button>
									<hr/>
								</form> 

								<div className="d-flex align-items-center">
									<ul className="social-icons social-icons-medium social-icons-clean-with-border social-icons-clean-with-border-border-grey social-icons-clean-with-border-icon-dark me-3 mb-0">
										
										<li className="social-icons-facebook">
											<a href="#" target="_blank" data-bs-toggle="tooltip" data-bs-animation="false" data-bs-placement="top" title="Share On Facebook">
												<i className="fab fa-facebook-f"></i>
											</a>
										</li>
										<li className="social-icons-googleplus">
											<a href="#" target="_blank" data-bs-toggle="tooltip" data-bs-animation="false" data-bs-placement="top" title="Share On Google+">
												<i className="fab fa-google-plus-g"></i>
											</a>
										</li>
										<li className="social-icons-twitter">
											<a href="#" target="_blank" data-bs-toggle="tooltip" data-bs-animation="false" data-bs-placement="top" title="Share On Twitter">
												<i className="fab fa-twitter"></i>
											</a>
										</li>
										<li className="social-icons-email">
											<a href="mailto:?Subject=Share This Page&amp;Body=I%20saw%20this%20and%20thought%20of%20you!%20 https://www.okler.net" data-bs-toggle="tooltip" data-bs-animation="false" data-bs-placement="top" title="Share By Email">
												<i className="far fa-envelope"></i>
											</a>
										</li>
									</ul>
									<a href="#" className="d-flex align-items-center text-decoration-none text-color-dark text-color-hover-primary font-weight-semibold text-2">
										<i className="far fa-heart me-1"></i> AJOUTER À LA LISTE DE SOUHAITS
									</a>
								</div>

							</div>

						</div>
					</div>

					<div className="row mb-4">
						<div className="col">
							<div id="description" className="tabs tabs-simple tabs-simple-full-width-line tabs-product tabs-dark mb-2">
								<ul className="nav nav-tabs justify-content-start">
									<li className="nav-item"><a className="nav-link active font-weight-bold text-3 text-uppercase py-2 px-3" href="#productDescription" data-bs-toggle="tab">Description</a></li>
									<li className="nav-item"><a className="nav-link font-weight-bold text-3 text-uppercase py-2 px-3" href="#productInfo" data-bs-toggle="tab">Informations Complémentaires</a></li>
									{/* <li className="nav-item"><a className="nav-link nav-link-reviews font-weight-bold text-3 text-uppercase py-2 px-3" href="#productReviews" data-bs-toggle="tab">Reviews (2)</a></li> */}
								</ul>
								<div className="tab-content p-0">
									<div className="tab-pane px-0 py-3 active" id="productDescription">
										<p>{product.description} </p>
										<p className="m-0">{product.description}</p>
									</div>
									<div className="tab-pane px-0 py-3" id="productInfo">
										<p className="m-0">{product.details}</p>
									</div>
									<div className="tab-pane px-0 py-3" id="productReviews">
										<ul className="comments">
											<li>
												<div className="comment">
													<div className="img-thumbnail border-0 p-0 d-none d-md-block">
														<img className="avatar rounded-circle" alt="" src="assets/img/avatars/avatar-2.jpg" />
													</div>
													<div className="comment-block">
														<div className="comment-arrow"></div>
														<span className="comment-by">
															<strong>Jack Doe</strong>
															<span className="float-end">
																<div className="pb-0 clearfix">
																	<div title="Rated 3 out of 5" className="float-start">
																		<input type="text" className="d-none" value="3" title="" data-plugin-star-rating data-plugin-options="{'displayOnly': true, 'color': 'primary', 'size':'xs'}" />
																	</div>

																	<div className="review-num">
																		<span className="count" itemprop="ratingCount">2</span> reviews
																	</div>
																</div>
															</span>
														</span>
														<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae, gravida pellentesque urna varius vitae.</p>
													</div>
												</div>
											</li>
											<li>
												<div className="comment">
													<div className="img-thumbnail border-0 p-0 d-none d-md-block">
														<img className="avatar rounded-circle" alt="" src="assets/img/avatars/avatar.jpg" />
													</div>
													<div className="comment-block">
														<div className="comment-arrow"></div>
														<span className="comment-by">
															<strong>John Doe</strong>
															<span className="float-end">
																<div className="pb-0 clearfix">
																	<div title="Rated 3 out of 5" className="float-start">
																		<input type="text" className="d-none" value="3" title="" data-plugin-star-rating data-plugin-options="{'displayOnly': true, 'color': 'primary', 'size':'xs'}" />
																	</div>

																	<div className="review-num">
																		<span className="count" itemprop="ratingCount">2</span> reviews
																	</div>
																</div>
															</span>
														</span>
														<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra odio, gravida urna varius vitae, gravida pellentesque urna varius vitae.</p>
													</div>
												</div>
											</li>
										</ul>
										<hr className="solid my-5" />
										<h4>Add a review</h4>
										<div className="row">
											<div className="col">

												<form action="" id="submitReview" method="post">
													<div className="row">
														<div className="form-group col pb-2">
															<label className="form-label required font-weight-bold text-dark">Rating</label>
															<input type="text" className="rating-loading" value="0" title="" data-plugin-star-rating data-plugin-options="{'color': 'primary', 'size':'sm'}" />
														</div>
													</div>
													<div className="row">
														<div className="form-group col-lg-6">
															<label className="form-label required font-weight-bold text-dark">Name</label>
															<input type="text" value="" data-msg-required="Please enter your name." maxlength="100" className="form-control" name="name" required />
														</div>
														<div className="form-group col-lg-6">
															<label className="form-label required font-weight-bold text-dark">Email Address</label>
															<input type="email" value="" data-msg-required="Please enter your email address." data-msg-email="Please enter a valid email address." maxlength="100" className="form-control" name="email" required />
														</div>
													</div>
													<div className="row">
														<div className="form-group col">
															<label className="form-label required font-weight-bold text-dark">Review</label>
															<textarea maxlength="5000" data-msg-required="Please enter your review." rows="8" className="form-control" name="review" id="review" required></textarea>
														</div>
													</div>
													<div className="row">
														<div className="form-group col mb-0">
															<input type="submit" value="Post Review" className="btn btn-primary btn-modern" data-loading-text="Loading..." />
														</div>
													</div>
												</form>
											</div>

										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div className="row">
						<div className="col">
							<h4 className="font-weight-semibold text-4 mb-3">PRODUITS CONNEXES</h4>
							<hr className="mt-0"/>
							<div className="products row">
								<div className="col">
									<div className="owl-carousel owl-theme nav-style-1 nav-outside nav-outside justify-content-between nav-dark mb-0" data-plugin-options="{'loop': false, 'autoplay': false, 'items': 4, 'nav': true, 'dots': false, 'margin': 20, 'autoplayHoverPause': true, 'autoHeight': true, 'stagePadding': '75', 'navVerticalOffset': '50px'}">

										{similar && similar.map((p) => 
										<div className="product mb-0" style={{  marginRight: 15 }}>
											<div className="product-thumb-info border-0 mb-3">

												<div className="product-thumb-info-badges-wrapper">
												{p.new_arrival === 1 && <span className="badge badge-ecommerce badge-success">NEW</span>}

												</div>

												<div className="addtocart-btn-wrapper">
													<button onClick={() => addProduct(p)} className="text-decoration-none addtocart-btn" title="Ajouter au panier">
														<i className="icons icon-bag"></i>
													</button>
												</div>

												<Link to={`/single/${p.id}`} className="quick-view text-uppercase font-weight-semibold text-2">
													Voir
												</Link>
												<Link to={`/single/${p.id}`}>
													<div className="product-thumb-info-image">
														<img alt="" className="img-fluid" src={p.image && `${AppConfig.apiUrlWeb}/${p.image}`} />

													</div>
												</Link>
											</div>
											<div className="d-flex justify-content-between">
												<div>
													<a href="#" className="d-block text-uppercase text-decoration-none text-color-default text-color-hover-primary line-height-1 text-0 mb-1">{p.categories && p.categories.map(c => c.name).toString()}</a>
													<h3 className="text-3-5 font-weight-medium font-alternative text-transform-none line-height-3 mb-0"><a href="shop-product-sidebar-right.html" className="text-color-dark text-color-hover-primary">Photo Camera</a></h3>
												</div>
												<a href="#" className="text-decoration-none text-color-default text-color-hover-dark text-4"><i className="far fa-heart"></i></a>
											</div>
											<div title="Rated 5 out of 5">
												<input type="text" className="d-none" value="5" title="" data-plugin-star-rating data-plugin-options="{'displayOnly': true, 'color': 'default', 'size':'xs'}" />
											</div>
											<p className="price text-5 mb-3">
												<span className="sale text-color-dark font-weight-semi-bold">{p.price_regular} Fcfa</span>
												{p.price_promo > 0 && <span className="amount">{p.price_promo} Fcfa</span>}
											</p>
										</div>)}

									</div>
								</div>
							</div>
						</div>
					</div>

					<hr className="my-5"/>

					<div className="products row">
						<div className="col-md-6 col-lg-3">
							<h4 className="font-weight-semibold text-4 mb-3">PRODUITS POPULAIRES</h4>

							{popular && popular.map((p) => 
							<div className="product row row-gutter-sm align-items-center mb-4">
								<div className="col-5 col-md-12 col-lg-5">
									<div className="product-thumb-info border-0">
										<Link to={`/single/${p.id}`}>
											<div className="product-thumb-info-image">
												<img alt="" className="img-fluid" src={p.image && `${AppConfig.apiUrlWeb}/${p.image}`} />

											</div>
										</Link>
									</div>
								</div>
								<div className="col-7 col-md-12 col-lg-7 ms-md-0 ms-lg-0 ps-lg-1 pt-1">
									<a href="#" className="d-block text-uppercase text-decoration-none text-color-default text-color-hover-primary line-height-1 text-0 mb-2">{p.categories && p.categories.map((x) => x.name).toString()}</a>
									<h3 className="text-3-5 font-weight-medium font-alternative text-transform-none line-height-3 mb-0"><Link to={`/single/${p.id}`} className="text-color-dark text-color-hover-primary text-decoration-none">{p.name}</Link></h3>
									<div title="Rated 5 out of 5">
										<input type="text" className="d-none" value="5" title="" data-plugin-star-rating data-plugin-options="{'displayOnly': true, 'color': 'dark', 'size':'xs'}"/>
									</div>
									<p className="price text-4 mb-0">
										{!p.price_promo && <span className="sale text-color-dark font-weight-semi-bold">{p.price_regular} Fcfa</span>}
										{p.price_promo > 0 && <span className="amount">{p.price_promo} Fcfa</span>}
									</p>
								</div>
							</div>)}

						</div>
						<div className="col-md-6 col-lg-3">
							<h4 className="font-weight-semibold text-4 mb-3">PRODUITS LES PLUS VENDUS</h4>

							{featured && featured.map((p) => 
							<div className="product row row-gutter-sm align-items-center mb-4">
								<div className="col-5 col-md-12 col-lg-5">
									<div className="product-thumb-info border-0">
										<Link to={`/single/${p.id}`}>
											<div className="product-thumb-info-image">
												<img alt="" className="img-fluid" src={p.image && `${AppConfig.apiUrlWeb}/${p.image}`} />

											</div>
										</Link>
									</div>
								</div>
								<div className="col-7 col-md-12 col-lg-7 ms-md-0 ms-lg-0 ps-lg-1 pt-1">
									<a href="#" className="d-block text-uppercase text-decoration-none text-color-default text-color-hover-primary line-height-1 text-0 mb-2">{p.categories && p.categories.map((x) => x.name).toString()}</a>
									<h3 className="text-3-5 font-weight-medium font-alternative text-transform-none line-height-3 mb-0"><Link to={`/single/${p.id}`} className="text-color-dark text-color-hover-primary text-decoration-none">{p.name}</Link></h3>
									<div title="Rated 5 out of 5">
										<input type="text" className="d-none" value="5" title="" data-plugin-star-rating data-plugin-options="{'displayOnly': true, 'color': 'dark', 'size':'xs'}"/>
									</div>
									<p className="price text-4 mb-0">
										{!p.price_promo && <span className="sale text-color-dark font-weight-semi-bold">{p.price_regular} Fcfa</span>}
										{p.price_promo > 0 && <span className="amount">{p.price_promo} Fcfa</span>}
									</p>
								</div>
							</div>)}

						</div>
						<div className="col-md-6 col-lg-3">
							<h4 className="font-weight-semibold text-4 mb-3">DERNIERS PRODUITS</h4>

							{newArrival && newArrival.map((p) => 
							<div className="product row row-gutter-sm align-items-center mb-4">
								<div className="col-5 col-md-12 col-lg-5">
									<div className="product-thumb-info border-0">
										<Link to={`/single/${p.id}`}>
											<div className="product-thumb-info-image">
												<img alt="" className="img-fluid" src={p.image && `${AppConfig.apiUrlWeb}/${p.image}`} />

											</div>
										</Link>
									</div>
								</div>
								<div className="col-7 col-md-12 col-lg-7 ms-md-0 ms-lg-0 ps-lg-1 pt-1">
									<a href="#" className="d-block text-uppercase text-decoration-none text-color-default text-color-hover-primary line-height-1 text-0 mb-2">{p.categories && p.categories.map((x) => x.name).toString()}</a>
									<h3 className="text-3-5 font-weight-medium font-alternative text-transform-none line-height-3 mb-0"><Link to={`/single/${p.id}`} className="text-color-dark text-color-hover-primary text-decoration-none">{p.name}</Link></h3>
									<div title="Rated 5 out of 5">
										<input type="text" className="d-none" value="5" title="" data-plugin-star-rating data-plugin-options="{'displayOnly': true, 'color': 'dark', 'size':'xs'}"/>
									</div>
									<p className="price text-4 mb-0">
										{!p.price_promo && <span className="sale text-color-dark font-weight-semi-bold">{p.price_regular} Fcfa</span>}
										{p.price_promo > 0 && <span className="amount">{p.price_promo} Fcfa</span>}
									</p>
								</div>
							</div>)}

						</div>
						<div className="col-md-6 col-lg-3">
							<h4 className="font-weight-semibold text-4 mb-3">PRODUITS LES MIEUX ÉVALUÉS</h4>

							{promo && promo.map((p) => 
							<div className="product row row-gutter-sm align-items-center mb-4">
								<div className="col-5 col-md-12 col-lg-5">
									<div className="product-thumb-info border-0">
										<Link to={`/single/${p.id}`}>
											<div className="product-thumb-info-image">
												<img alt="" className="img-fluid" src={p.image && `${AppConfig.apiUrlWeb}/${p.image}`} />

											</div>
										</Link>
									</div>
								</div>
								<div className="col-7 col-md-12 col-lg-7 ms-md-0 ms-lg-0 ps-lg-1 pt-1">
									<a href="#" className="d-block text-uppercase text-decoration-none text-color-default text-color-hover-primary line-height-1 text-0 mb-2">{p.categories && p.categories.map((x) => x.name).toString()}</a>
									<h3 className="text-3-5 font-weight-medium font-alternative text-transform-none line-height-3 mb-0"><Link to={`/single/${p.id}`} className="text-color-dark text-color-hover-primary text-decoration-none">{p.name}</Link></h3>
									<div title="Rated 5 out of 5">
										<input type="text" className="d-none" value="5" title="" data-plugin-star-rating data-plugin-options="{'displayOnly': true, 'color': 'dark', 'size':'xs'}"/>
									</div>
									<p className="price text-4 mb-0">
										{!p.price_promo && <span className="sale text-color-dark font-weight-semi-bold">{p.price_regular} Fcfa</span>}
										{p.price_promo > 0 && <span className="amount">{p.price_promo} Fcfa</span>}
									</p>
								</div>
							</div>)}

						</div>
					</div>
				</div>
     );
}
 
export default SingleProduct;