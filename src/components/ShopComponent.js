import React, { useEffect, useState } from 'react';
import { Link } from "react-router-dom";
import Categorie from './Categorie'
import { getResource } from "../config/Api";
import Product from './Product';
import { onServerError } from '../config/Helper';
import Loading from './Loading';


const Pagination = ({
    currentPage,
    totalPages,
    onNextPage,
    onPreviousPage,
    pageNumbers,
  }) => {
    return (
        <ul className="pagination float-end">
            <li className="page-item"><a className="page-link" disabled={currentPage === 1}
            onClick={onPreviousPage}><i className="fas fa-angle-left"></i></a></li>
                            {pageNumbers}
            <li className="page-item"><a className="page-link" disabled={currentPage === totalPages}
            onClick={onNextPage}><i className="fas fa-angle-right"></i></a></li>
        </ul>
     
    );
  };

const ShopComponent = () => {

    const [product, setProduct] = useState([])
    const [loading, setLoading] = useState(false)
    const [currentPage, setCurrentPage] = useState(1)
    const [itemsPerPage, setItemsPerPage] = useState(9);

    const products = () => {
        getResource('products').then((res) => {
            setLoading(true)
            console.log(res.data)
            setProduct(res.data["data"])
            setCurrentPage(res.data.meta.current_page)
            setItemsPerPage(res.data.meta.per_page)
            setLoading(false)
        }).catch((e) => {
            onServerError(e.message)
        }).finally(() => {
            setLoading(false)
        })
    }

    const getItemsForCurrentPage = () => {
        const startIndex = (currentPage - 1) * itemsPerPage;
        const endIndex = startIndex + itemsPerPage;
        return product.slice(startIndex, endIndex);
      };
      const totalPages = Math.ceil(product.length / itemsPerPage);

    useEffect(() => {
        products()
        getItemsForCurrentPage()
    }, [])


    const updateCurrentPage = (page) => {
        setCurrentPage(page);
      };
    
      // Fonction pour passer à la page suivante
      const nextPage = () => {
        if (currentPage < totalPages) {
          console.log(currentPage + 1);
          updateCurrentPage(currentPage + 1);
        }
      };
    
      // Fonction pour passer à la page précédente
      const previousPage = () => {
        if (currentPage > 1) {
          updateCurrentPage(currentPage - 1);
        }
      };
    
      const renderPageNumbers = () => {
        const pageNumbers = [];
    
        for (let i = 1; i <= totalPages; i++) {
          pageNumbers.push(
            <li
              key={i}
              className={currentPage === i ? "page-item active" : "page-item"}
            >
              <a className="page-link" href="#" onClick={() => setCurrentPage(i)}>
                {i}
              </a>
            </li>
          );
        }
    
        return pageNumbers;
      };
    return ( 
        <div class="container">
        <div class="row">
            <Categorie/>
        <div className="col-lg-9 order-1 order-lg-2">

            <div className="masonry-loader ">
                <div className="row products product-thumb-info-list" dataPluginMasonry dataPluginOptions="{'layoutMode': 'fitRows'}">

                            {loading ? <Loading/> : getItemsForCurrentPage().map((x) => <Product item={x}/> )}

                </div>
                <div className="row mt-4">
                    <div className="col">
                        <Pagination
                        currentPage={currentPage}
                        totalPages={totalPages}
                        onNextPage={nextPage}
                        onPreviousPage={previousPage}
                        pageNumbers={renderPageNumbers}
                        />
                    </div>
                </div>
            </div>
        </div>
        </div>
        </div>
     );
}
 
export default ShopComponent