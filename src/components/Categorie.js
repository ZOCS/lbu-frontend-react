import React, {useState, useEffect} from 'react';
import { getResource } from '../config/Api';


const Categorie = () => {

	const [categories, setCategories] = useState([])

	const getCategories = () => {
		getResource('categories').then((res) => {
			console.log(res.data)
			setCategories(res.data['data']);
		})
	}

	useEffect(() => {
		getCategories()
	}, [])

    return ( 
        <div className="col-lg-3 order-2 order-lg-1">
							<aside className="sidebar">
								<form action="page-search-results.html" method="get">
									<div className="input-group mb-3 pb-1">
										<input className="form-control text-1" placeholder="Search..." name="s" id="s" type="text"/>
										<button type="submit" className="btn btn-dark text-1 p-2"><i className="fas fa-search m-2"></i></button>
									</div>
								</form>
								<h5 className="font-weight-semi-bold pt-3">Categories</h5>
								<ul className="nav nav-list flex-column">
									{categories && categories.map(c =>
										<li className="nav-item"><a className="nav-link" href="#">{c.name}</a></li>)}
									
								</ul>
								<h5 className="font-weight-semi-bold pt-5">Tags</h5>
								<div className="mb-3 pb-1">
									<a href="#"><span className="badge badge-dark badge-sm rounded-pill text-uppercase px-2 py-1 me-1">Nike</span></a>
									<a href="#"><span className="badge badge-dark badge-sm rounded-pill text-uppercase px-2 py-1 me-1">Travel</span></a>
									<a href="#"><span className="badge badge-dark badge-sm rounded-pill text-uppercase px-2 py-1 me-1">Sport</span></a>
									<a href="#"><span className="badge badge-dark badge-sm rounded-pill text-uppercase px-2 py-1 me-1">TV</span></a>
									<a href="#"><span className="badge badge-dark badge-sm rounded-pill text-uppercase px-2 py-1 me-1">Books</span></a>
									<a href="#"><span className="badge badge-dark badge-sm rounded-pill text-uppercase px-2 py-1 me-1">Tech</span></a>
									<a href="#"><span className="badge badge-dark badge-sm rounded-pill text-uppercase px-2 py-1 me-1">Adidas</span></a>
									<a href="#"><span className="badge badge-dark badge-sm rounded-pill text-uppercase px-2 py-1 me-1">Promo</span></a>
									<a href="#"><span className="badge badge-dark badge-sm rounded-pill text-uppercase px-2 py-1 me-1">Reading</span></a>
									<a href="#"><span className="badge badge-dark badge-sm rounded-pill text-uppercase px-2 py-1 me-1">Social</span></a>
									<a href="#"><span className="badge badge-dark badge-sm rounded-pill text-uppercase px-2 py-1 me-1">Books</span></a>
									<a href="#"><span className="badge badge-dark badge-sm rounded-pill text-uppercase px-2 py-1 me-1">Tech</span></a>
									<a href="#"><span className="badge badge-dark badge-sm rounded-pill text-uppercase px-2 py-1 me-1">New</span></a>
								</div>
								<div className="row mb-5">
									<div className="col">
										<h5 className="font-weight-semi-bold pt-5">Top Rated Products</h5>
										<div className="product row row-gutter-sm align-items-center mb-4">
											<div className="col-5 col-lg-5">
												<div className="product-thumb-info border-0">
													<a href="shop-product-sidebar-left.html">
														<div className="product-thumb-info-image">
															<img alt="" className="img-fluid" src="assets/img/products/product-grey-6.jpg"/>
														</div>
													</a>
												</div>
											</div>
											<div className="col-7 col-lg-7 ms-md-0 ms-lg-0 ps-lg-1 pt-1">
												<a href="#" className="d-block text-uppercase text-decoration-none text-color-default text-color-hover-primary line-height-1 text-0 mb-2">hat</a>
												<h3 className="text-3-5 font-weight-medium font-alternative text-transform-none line-height-3 mb-0"><a href="shop-product-sidebar-right.html" className="text-color-dark text-color-hover-primary text-decoration-none">Blue Hat</a></h3>
												<div title="Rated 5 out of 5">
													<input type="text" className="d-none" value="5" title="" data-plugin-star-rating data-plugin-options="{'displayOnly': true, 'color': 'dark', 'size':'xs'}"/>
												</div>
												<p className="price text-4 mb-0">
													<span className="sale text-color-dark font-weight-semi-bold">$299,00</span>
													<span className="amount">$289,00</span>
												</p>
											</div>
										</div>
										<div className="product row row-gutter-sm align-items-center mb-4">
											<div className="col-5 col-lg-5">
												<div className="product-thumb-info border-0">
													<a href="shop-product-sidebar-left.html">
														<div className="product-thumb-info-image">
															<img alt="" className="img-fluid" src="assets/img/products/product-grey-8.jpg"/>
														</div>
													</a>
												</div>
											</div>
											<div className="col-7 col-lg-7 ms-md-0 ms-lg-0 ps-lg-1 pt-1">
												<a href="#" className="d-block text-uppercase text-decoration-none text-color-default text-color-hover-primary line-height-1 text-0 mb-2">accessories</a>
												<h3 className="text-3-5 font-weight-medium font-alternative text-transform-none line-height-3 mb-0"><a href="shop-product-sidebar-right.html" className="text-color-dark text-color-hover-primary text-decoration-none">Adventurer Bag</a></h3>
												<div title="Rated 5 out of 5">
													<input type="text" className="d-none" value="5" title="" data-plugin-star-rating data-plugin-options="{'displayOnly': true, 'color': 'dark', 'size':'xs'}"/>
												</div>
												<p className="price text-4 mb-0">
													<span className="sale text-color-dark font-weight-semi-bold">$99,00</span>
													<span className="amount">$79,00</span>
												</p>
											</div>
										</div>
										<div className="product row row-gutter-sm align-items-center mb-4">
											<div className="col-5 col-lg-5">
												<div className="product-thumb-info border-0">
													<a href="shop-product-sidebar-left.html">
														<div className="product-thumb-info-image">
															<img alt="" className="assets/img-fluid" src="img/products/product-grey-9.jpg"/>
														</div>
													</a>
												</div>
											</div>
											<div className="col-7 col-lg-7 ms-md-0 ms-lg-0 ps-lg-1 pt-1">
												<a href="#" className="d-block text-uppercase text-decoration-none text-color-default text-color-hover-primary line-height-1 text-0 mb-2">sports</a>
												<h3 className="text-3-5 font-weight-medium font-alternative text-transform-none line-height-3 mb-0"><a href="shop-product-sidebar-right.html" className="text-color-dark text-color-hover-primary text-decoration-none">Baseball Ball</a></h3>
												<div title="Rated 5 out of 5">
													<input type="text" className="d-none" value="5" title="" data-plugin-star-rating data-plugin-options="{'displayOnly': true, 'color': 'dark', 'size':'xs'}"/>
												</div>
												<p className="price text-4 mb-0">
													<span className="sale text-color-dark font-weight-semi-bold">$399,00</span>
													<span className="amount">$299,00</span>
												</p>
											</div>
										</div>
									</div>
								</div>
							</aside>
						</div>
     );
}
 
export default Categorie;