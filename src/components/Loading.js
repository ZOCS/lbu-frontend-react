import React from 'react';
import { ColorRing } from  'react-loader-spinner'

const Loading = () => {
    return (
      <div className="container">
        <ColorRing
          visible={true}
          height="50"
          width="50"
          ariaLabel="blocks-loading"
          wrapperStyle={{}}
          wrapperClass="blocks-wrapper"
          colors={["#34C38F", "#34C38F", "#34C38F", "#34C38F", "#34C38F"]}
        />
      </div>
    );
}
export default Loading;