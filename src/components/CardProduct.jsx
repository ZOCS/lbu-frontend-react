/* eslint-disable jsx-a11y/anchor-is-valid */
import AppConfig from "../config/AppConfig";
import { Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { addCart } from "../store/action/action";
import { onServerSuccess } from "../config/Helper";

const CardProduct = ({ item }) => {
  const dispatch = useDispatch();

  const addProduct = (product) => {
    // console.log(product)
    dispatch(addCart(product));
    onServerSuccess(`${product.name} ajouter au panier`);
  };
  return (
    <div className="owl-item">
    <div className="card " style={{ width: '15rem', padding: '5px', boxShadow: 8, marginRight: '10px' }}>
    <div className="product mb-0">
      <div className="product-thumb-info border-0 mb-3">
        <div className="product-thumb-info-badges-wrapper">
          <span className="badge badge-ecommerce badge-success">
            {item && item.new_arrival && "NEW"}
          </span>
          <span className="badge badge-ecommerce badge-danger">
            {item && item.promo && item.taux_promo + "% OFF"}
          </span>
        </div>

        <div className="addtocart-btn-wrapper">
          <button
            onClick={() => addProduct(item)}
            className="text-decoration-none addtocart-btn"
            title="Ajouter au panier"
          >
            <i className="icons icon-bag"></i>
          </button>
        </div>

        <Link
          to="/single"
          className="quick-view text-uppercase font-weight-semibold text-2"
        >
          Voir
        </Link>
        <Link to="/single">
          <div className="product-thumb-info-image">
            {/* <img alt="" class="img-fluid" src="assets/img/products/product-grey-1.jpg" /> */}
            <img
              alt=""
              className="img-fluid"
              src={item.image && `${AppConfig.apiUrlWeb}/${item.image}`}
            />
          </div>
        </Link>
      </div>
      <div className="d-flex justify-content-between">
        <div>
          <a
            href="#"
            className="d-block text-uppercase text-decoration-none text-color-default text-color-hover-primary line-height-1 text-0 mb-1"
          >
            {item.categories && item.categories.map((x) => x.name).toString('-')}
          </a>
          <h3 className="text-3-5 font-weight-medium font-alternative text-transform-none line-height-3 mb-0">
            <a
              href="shop-product-sidebar-right.html"
              className="text-color-dark text-color-hover-primary"
            >
              {item.name}
            </a>
          </h3>
        </div>
        <a
          href="#"
          className="text-decoration-none text-color-default text-color-hover-dark text-4"
        >
          <i className="far fa-heart"></i>
        </a>
      </div>
      <div title="Rated 5 out of 5">
        <input
          type="text"
          className="d-none"
          value="5"
          title=""
          data-plugin-star-rating
          data-plugin-options="{'displayOnly': true, 'color': 'default', 'size':'xs'}"
        />
      </div>
      <p className="price text-5 mb-3">
        {item.price_promo >= 0 ? (
          <>
            <span className="sale text-color-dark font-weight-semi-bold">
              {item.price_promo}
            </span>
            <span className="amount">{item.price_regular}</span>
          </>
        ) : (
          <>
            <span className="sale text-color-dark font-weight-semi-bold">
              {item.price_regular}
            </span>
          </>
        )}
      </p>
    </div>
    </div>
    </div>
  );
};

export default CardProduct;
