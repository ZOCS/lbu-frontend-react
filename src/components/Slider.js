import React, {useState, useEffect} from "react";
import { Link } from "react-router-dom";

const Slider = () => {
    return ( 
        <>
                        <div id="carouselExampleIndicators" className="carousel slide" data-bs-ride="carousel">
                    <div className="carousel-indicators">
                        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" className="active" aria-current="true" aria-label="Slide 1"></button>
                        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
                        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
                    </div>
                    <div className="carousel-inner">
                        <div className="carousel-item active">
                        <img src="assets/img/accueil_slide_1.jpg" className="d-block w-100" style={{ height: '35rem' }} alt="..."/>
                        </div>
                        <div className="carousel-item">
                        <img src="assets/img/accueil_slide_2.png" className="d-block w-100" alt="..." style={{ height: '35rem' }}/>
                        </div>
                        {/* <div className="carousel-item">
                        <img src="assets/img/slides/slide-bg-2.jpg"  className="d-block w-100" alt="..."/>
                        </div> */}
                    </div>
                    <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                        <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span className="visually-hidden">Previous</span>
                    </button>
                    <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                        <span className="carousel-control-next-icon" aria-hidden="true"></span>
                        <span className="visually-hidden">Next</span>
                    </button>
                    </div>
                    <div className="home-intro bg-primary" id="home-intro">
                        <div className="container">

                            <div className="row align-items-center">
                                <div className="col-lg-8">
                                    <p>
                                    <span className="highlighted-word">Bienvenu à LBU,</span>  la librairie unique comme vous 
                                        <span>La première librairie en ligne au Bénin</span>
                                    </p>
                                </div>
                                <div className="col-lg-4">
                                    <div className="get-started text-start text-lg-end">
                                        <Link to="/shop" className="btn btn-dark btn-lg text-3 font-weight-semibold px-4 py-3">Commencez maintenant</Link>
                                        
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
            </>
     );
}
 
export default Slider;