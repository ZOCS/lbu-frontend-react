import React from 'react';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';

export default function Header() {
const state = useSelector((state) => state.root.handleCart);
  return (
    <header id="header" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': false, 'stickyStartAt': 45, 'stickySetTop': '-45px', 'stickyChangeLogo': true}">
				<div className="header-body">
					<div className="header-container container">
						<div className="header-row">
							<div className="header-column">
								<div className="header-row">
									<div className="header-logo">
										<a href="index.html">
											<img alt="Porto" width="70" height="70" data-sticky-width="82" data-sticky-height="60" data-sticky-top="25" src="assets/img/logolbu.png"/>
										</a>
									</div>
								</div>
							</div>
							<div className="header-column justify-content-end">
								<div className="header-row pt-3">
									<nav className="header-nav-top">
										<ul className="nav nav-pills">
											<li className="nav-item nav-item-anim-icon d-none d-md-block">
												<a className="nav-link ps-0" to="/login"><i className="fas fa-angle-right"></i> Se connecter</a>
											</li>
											<li className="nav-item nav-item-anim-icon d-none d-md-block">
												<Link className="nav-link" to="/register"><i className="fas fa-angle-right"></i> S'inscrire</Link>
											</li>
											<li className="nav-item nav-item-left-border nav-item-left-border-remove nav-item-left-border-md-show">
												<span className="ws-nowrap"><i className="fas fa-phone"></i> (123) 456-789</span>
											</li>
										</ul>
									</nav>
									<div className="header-nav-features">
										<div className="header-nav-feature header-nav-features-search d-inline-flex">
											<a href="#" className="header-nav-features-toggle text-decoration-none" data-focus="headerSearch"><i className="fas fa-search header-nav-top-icon"></i></a>
											<div className="header-nav-features-dropdown" id="headerTopSearchDropdown">
												<form role="search" action="page-search-results.html" method="get">
													<div className="simple-search input-group">
														<input className="form-control text-1" id="headerSearch" name="q" type="search" value="" placeholder="Rechercher..."/>
														<button className="btn" type="submit">
															<i className="fas fa-search header-nav-top-icon"></i>
														</button>
													</div>
												</form>
											</div>
										</div>
										<div className="header-nav-feature header-nav-features-cart d-inline-flex ms-2">
											<a href="/cart" className="header-nav-features-toggle">
												<img src="assets/img/icons/icon-cart.svg" width="14" alt="" className="header-nav-top-icon-img"/>
												<span className="cart-info d-none">
													<span className="cart-qty">{state && state.length}</span>
												</span>
											</a>
											<div className="header-nav-features-dropdown" id="headerTopCartDropdown">
												<ol className="mini-products-list">
													<li className="item">
														<a href="#" title="Camera X1000" className="product-image"><img src="assets/img/products/product-1.jpg" alt="Camera X1000"/></a>
														<div className="product-details">
															<p className="product-name">
																<a href="#">Camera X1000 </a>
															</p>
															<p className="qty-price">
																 1X <span className="price">$890</span>
															</p>
															<a href="#" title="Remove This Item" className="btn-remove"><i className="fas fa-times"></i></a>
														</div>
													</li>
												</ol>
												<div className="totals">
													<span className="label">Total:</span>
													<span className="price-total"><span className="price">$890</span></span>
												</div>
												<div className="actions">
													<a className="btn btn-dark" href="#">View Cart</a>
													<a className="btn btn-primary" href="#">Checkout</a>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div className="header-row">
									<div className="header-nav pt-1">
										<div className="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1">
											<nav className="collapse">
												<ul className="nav nav-pills" id="mainNav">
													<li className="dropdown">
														<Link className="dropdown-item  active" to="/">
															Accueil
														</Link>
													</li>
													<li className="dropdown">
														<Link className="dropdown-item" to="/shop">
															Boutique
														</Link>
													</li>
													<li className="dropdown">
														<a className="dropdown-item " href="#">
															Contact
														</a>
													</li>
												</ul>
											</nav>
										</div>
										<ul className="header-social-icons social-icons d-none d-sm-block">
											<li className="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i className="fab fa-facebook-f"></i></a></li>
											<li className="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i className="fab fa-twitter"></i></a></li>
											<li className="social-icons-linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i className="fab fa-linkedin-in"></i></a></li>
										</ul>
										<button className="btn header-btn-collapse-nav" data-bs-toggle="collapse" data-bs-target=".header-nav-main nav">
											<i className="fas fa-bars"></i>
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>
  )
}
