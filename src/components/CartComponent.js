import React, { useState, useEffect } from 'react';
import { Link,  } from "react-router-dom";
import { useSelector, useDispatch } from 'react-redux';
import AppConfig from "../config/AppConfig";
import { addCart, delCart, resetProduct } from '../store/action/action';
import { getResource } from '../config/Api';
import { onServerError, onServerSuccess } from '../config/Helper';

const CartComponent = () => {
	const cart = useSelector((state) => state.root.cart);
	const [products, setProducts] = useState([]);
	const dispatch = useDispatch();
	const getTotalAmount = () =>{
		let amount = 0;
		cart.map((c) => {
			// let itemInfo = cart.find((p) => p.id === Number(i));
				amount += c.qty * c.price_regular
		})
				
		return amount;
	}

	const addProduct = (product) => {
        // console.log(product)
        dispatch(addCart(product));
        onServerSuccess(`${product.name} ajouter au panier`)
    }
	const _init_ = async() => {
		await getResource("products").then((res) => {
			// console.log(res.data)
			setProducts(res.data['data']);
		}).catch(e => {
			if(e.response.status === 500){
				onServerError(e.response.message)
			}else if(e.response.status === 404){
				onServerError(e.response.message)
			}
		})
	}

	const handleQuantity = (product) => {
		dispatch(addCart(product))

	}

	const handleDelQuantity = (product) => {
		dispatch(delCart(product))

	}

	useEffect(() => {
		_init_()
		console.log(cart);
		getTotalAmount();

	}, [dispatch, cart, handleQuantity, handleDelQuantity])

    return ( 
        <div className="container">

					<div className="row">
						<div className="col">
							<ul className="breadcrumb font-weight-bold text-6 justify-content-center my-5">
								<li className="text-transform-none me-2">
									<Link to="/cart" className="text-decoration-none text-color-primary">Shopping Cart</Link>
								</li>
								<li className="text-transform-none text-color-grey-lighten me-2">
									<Link to="/checkout" className="text-decoration-none text-color-grey-lighten text-color-hover-primary">Checkout</Link>
								</li>
								<li className="text-transform-none text-color-grey-lighten">
									<Link href="/confirm" className="text-decoration-none text-color-grey-lighten text-color-hover-primary">Order Complete</Link>
								</li>
							</ul>
						</div>
					</div>
					{ cart.length > 0 ? <div className="row pb-4 mb-5">
						<div className="col-lg-8 mb-5 mb-lg-0">
							<form method="post" action="">
								<div className="table-responsive">
									<table className="shop_table cart">
										<thead>
											<tr className="text-color-dark">
												<th className="product-thumbnail" width="15%">
													Image
												</th>
												<th className="product-name text-uppercase" width="30%">
													Produit
												</th>
												<th className="product-price text-uppercase" width="15%">
													Prix U.
												</th>
												<th className="product-quantity text-uppercase" width="20%">
													Quantité
												</th>
												<th className="product-subtotal text-uppercase text-end" width="20%">
													Soustotal
												</th>
											</tr>
										</thead>
										<tbody>

									{cart && cart.map((x) =>
										<tr className="cart_table_item" key={x.id}>
											<td className="product-thumbnail">
												<div className="product-thumbnail-wrapper">
													<Link onClick={() => { dispatch(resetProduct(x))}} className="product-thumbnail-remove" title="Remove Product">
														<i className="fas fa-times"></i>
													</Link>
													<a href="shop-product-sidebar-right.html" className="product-thumbnail-image" title="Photo Camera">
														<img width="90" height="90" alt="" className="img-fluid" src={x.image && `${AppConfig.apiUrlWeb}/${x.image}`} />
													</a>
												</div>
											</td>
											<td className="product-name">
												<a href="shop-product-sidebar-right.html" className="font-weight-semi-bold text-color-dark text-color-hover-primary text-decoration-none">{x.name}</a>
											</td>
											<td className="product-price">
												<span className="amount font-weight-medium text-color-grey">{x.price_regular}</span>
											</td>
											<td className="product-quantity">
												<div className="quantity float-none m-0">
													<button type="button" onClick={() => dispatch(delCart(x))} className="minus text-color-hover-light bg-color-hover-primary border-color-hover-primary" >-</button>
													<input type="number" className="input-text qty text" value={x.qty}  name="qty" min="1" step={1} readOnly />
													<button type="button" onClick={() => dispatch(addCart(x))} className="plus text-color-hover-light bg-color-hover-primary border-color-hover-primary" >+</button>
												</div>
											</td>
											<td className="product-subtotal text-end">
												<span className="amount text-color-dark font-weight-bold text-4">{ x.qty * x.price_regular}</span>
											</td>
										</tr>
									)}

											
											<tr>
												<td colspan="5">
													<div className="row justify-content-between mx-0">
														<div className="col-md-auto px-0 mb-3 mb-md-0">
															<div className="d-flex align-items-center">
																<input type="text" className="form-control h-auto border-radius-0 line-height-1 py-3" name="couponCode" placeholder="Coupon Code" />
																<button type="submit" className="btn btn-light btn-modern text-color-dark bg-color-light-scale-2 text-color-hover-light bg-color-hover-primary text-uppercase text-3 font-weight-bold border-0 border-radius-0 ws-nowrap btn-px-4 py-3 ms-2">Apply Coupon</button>
															</div>
														</div>
														<div className="col-md-auto px-0">
															<button type="submit" className="btn btn-light btn-modern text-color-dark bg-color-light-scale-2 text-color-hover-light bg-color-hover-primary text-uppercase text-3 font-weight-bold border-0 border-radius-0 btn-px-4 py-3">Update Cart</button>
														</div>
													</div>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</form>
						</div>
						<div className="col-lg-4 position-relative">
							<div className="card border-width-3 border-radius-0 border-color-hover-dark" data-plugin-sticky data-plugin-options="{'minWidth': 991, 'containerSelector': '.row', 'padding': {'top': 85}}">
								<div className="card-body">
									<h4 className="font-weight-bold text-uppercase text-4 mb-3">Total Panier</h4>
									<table className="shop_table cart-totals mb-4">
										<tbody>
											<tr className="cart-subtotal">
												<td className="border-top-0">
													<strong className="text-color-dark">Sous-total</strong>
												</td>
												<td className="border-top-0 text-end">
													<strong><span className="amount font-weight-medium">{`${ getTotalAmount() } XOF`}</span></strong>
												</td>
											</tr>
											<tr className="shipping">
												<td colspan="2">
													<strong className="d-block text-color-dark mb-2">Livraison</strong>

													<div className="d-flex flex-column">
														<label className="d-flex align-items-center text-color-grey mb-0" for="shipping_method1">
															<input id="shipping_method1" type="radio" className="me-2" name="shipping_method" value="free" checked />
															Free Shipping
														</label>
														<label className="d-flex align-items-center text-color-grey mb-0" for="shipping_method2">
															<input id="shipping_method2" type="radio" className="me-2" name="shipping_method" value="local-pickup" />
															Local Pickup
														</label>
														<label className="d-flex align-items-center text-color-grey mb-0" for="shipping_method3">
															<input id="shipping_method3" type="radio" className="me-2" name="shipping_method" value="flat-rate" />
															Flat Rate: $5.00
														</label>
													</div>
												</td>
											</tr>
											<tr className="total">
												<td>
													<strong className="text-color-dark text-3-5">Total</strong>
												</td>
												<td className="text-end">
													<strong className="text-color-dark"><span className="amount text-color-dark text-5">{`${ getTotalAmount() } XOF`}</span></strong>
												</td>
											</tr>
										</tbody>
									</table>
									<Link to="/checkout" className="btn btn-dark btn-modern w-100 text-uppercase bg-color-hover-primary border-color-hover-primary border-radius-0 text-3 py-3">Passer à la caisse <i className="fas fa-arrow-right ms-2"></i></Link>
								</div>
							</div>
						</div>
					</div> : 
					<div  className="font-weight-semi-bold justify-content-center text-color-dark text-color-hover-primary text-decoration-none row pb-4 mb-5 ml-20" style={{ textAlign: 'center', justifyContent: 'center' }}><span style={{  color: 'red', fontSize: 30, display: 'flex', textAlign: 'center' }}>Votre panier est vide</span></div>}
					<div className="row">
						<div className="col">
							<h4 className="font-weight-semibold text-4 mb-3">Produits similaires</h4>
							<hr className="mt-0"/>
							<div className="products row">
								<div className="col">
									<div className="owl-carousel owl-theme show-nav-title show-nav-title-both-sides owl-loaded owl-drag owl-carousel-init" data-plugin-options="{'loop': false, 'autoplay': true, 'items': 2, 'nav': true, 'dots': false, 'margin': 20, 'autoplayHoverPause': true, 'autoHeight': true, 'stagePadding': '75', 'navVerticalOffset': '50px'}">

									{products && products.map((p) => 
										<div className="product mb-0" style={{  marginRight: 15 }}>
											<div className="product-thumb-info border-0 mb-3">

												<div className="product-thumb-info-badges-wrapper">
												{p.new_arrival === 1 && <span className="badge badge-ecommerce badge-success">NEW</span>}

												</div>

												<div className="addtocart-btn-wrapper">
													<button onClick={() => addProduct(p)} className="text-decoration-none addtocart-btn" title="Ajouter au panier">
														<i className="icons icon-bag"></i>
													</button>
												</div>

												<Link to={`/single/${p.id}`} className="quick-view text-uppercase font-weight-semibold text-2">
													Voir
												</Link>
												<Link to={`/single/${p.id}`}>
													<div className="product-thumb-info-image">
														<img alt="" className="img-fluid" src={p.image && `${AppConfig.apiUrlWeb}/${p.image}`} />

													</div>
												</Link>
											</div>
											<div className="d-flex justify-content-between">
												<div>
													<a href="#" className="d-block text-uppercase text-decoration-none text-color-default text-color-hover-primary line-height-1 text-0 mb-1">{p.categories && p.categories.map(c => c.name).toString()}</a>
													<h3 className="text-3-5 font-weight-medium font-alternative text-transform-none line-height-3 mb-0"><a href="shop-product-sidebar-right.html" className="text-color-dark text-color-hover-primary">Photo Camera</a></h3>
												</div>
												<a href="#" className="text-decoration-none text-color-default text-color-hover-dark text-4"><i className="far fa-heart"></i></a>
											</div>
											<div title="Rated 5 out of 5">
												<input type="text" className="d-none" value="5" title="" data-plugin-star-rating data-plugin-options="{'displayOnly': true, 'color': 'default', 'size':'xs'}" />
											</div>
											<p className="price text-5 mb-3">
												<span className="sale text-color-dark font-weight-semi-bold">{p.price_regular} Fcfa</span>
												{p.price_promo > 0 && <span className="amount">{p.price_promo} Fcfa</span>}
											</p>
										</div>)}

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
     );
}
 
export default CartComponent;