import React, { Children, useState } from 'react';
import Header from './Header';
import Footer from './Footer';
import Slider from './Slider';
import HeaderShop from './HeaderShop';


const AppBody = ({children, slide, shop, clas}) => {


    return ( 
        <div className="body">
            {shop  ? <HeaderShop/> : <Header/>}
            <div role="main" className={"main " + clas}>
                {slide && <Slider />}
                {children}
            </div>
            <Footer/>
        </div>
     );
}
 
export default AppBody;