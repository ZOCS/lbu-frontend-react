import React, { useState, useEffect } from 'react';
import AppBody from "../components/AppBody";
import {postResource} from '../config/Api'
import { toast, ToastContainer } from 'react-toastify';
import "react-toastify/dist/ReactToastify.css";
import { onServerSuccess, onServerError, storeData, validatorStringMinimum, validatorEmail, getData } from '../config/Helper';

const Login = () => {


	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [errors, setErrors] = React.useState({});

	 const handleError = (error, input) => {
    setErrors(prevState => ({ ...prevState, [input]: error }));
  };

	const validate = (e) => {
		e.preventDefault();
		let isValid = true;

		if (!validatorStringMinimum(8, password)) {
      handleError("Votre mot de passe doit contenir minimum de 8 caractères. ", 'password');
      isValid = false;
    }

    if (!validatorEmail(email)) {
      handleError("Votre mail est obligatoire", 'email');
      isValid = false;
    }
        if (isValid) {
        login()
    }
	}

	const login = () => {
		let data = {email, password}
		postResource('/login', data).then(res => {
			console.log(res.data)
			onServerSuccess("Connexion reussie !!!")
		}).catch ((e) => {
            onServerError(e.message) 
        }).finally(() => {
        })
	}

	// const handleInput = (e) => {
	// 	const { name, value } = e.target;
	
	// 	setData((s) => ({
	// 	  ...s,
	// 	  [name]: value,
	// 	}));
	//   };
	

    return ( 
        <AppBody shop>
            <div class="container py-4">
				<ToastContainer/>
					<div class="row justify-content-center">
						<div class="col-md-6 col-lg-5 mb-5 mb-lg-0">
							<h2 class="font-weight-bold text-5 mb-0">Se connecter</h2>
							<form onSubmit={e => validate(e)} class="needs-validation">
								<div class="row">
									<div class="form-group col">
										<label class="form-label text-color-dark text-3">Adresse Email <span class="text-color-danger">*</span></label>
										<input type="text" style={{  borderColor: errors.email &&'red'  }} value={email} onChange={e => setEmail(e.target.value)} class="form-control form-control-lg text-4" />
										<span style={{ color: 'red' }}>{errors.email}</span>
									</div>
								</div>
								<div class="row">
									<div class="form-group col">
										<label class="form-label text-color-dark text-3">Mot de passe <span class="text-color-danger">*</span></label>
										<input type="password" style={{  borderColor: errors.password &&'red'  }}  value={password} onChange={e => setPassword(e.target.value)} class="form-control form-control-lg text-4" />
										<span style={{ color: 'red' }}>{errors.password}</span>
									</div>
								</div>
								<div class="row justify-content-between">
									<div class="form-group col-md-auto">
										<div class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input" id="rememberme" />
											<label class="form-label custom-control-label cur-pointer text-2" for="rememberme">Souviens-toi de moi</label>
										</div>
									</div>
									<div class="form-group col-md-auto">
										<a class="text-decoration-none text-color-dark text-color-hover-primary font-weight-semibold text-2" href="#">Mot de passe oublié?</a>
									</div>
								</div>
								<div class="row">
									<div class="form-group col">
										<button type="submit" class="btn btn-dark btn-modern w-100 text-uppercase rounded-0 font-weight-bold text-3 py-3" data-loading-text="Loading...">Se connecter</button>
										<div class="divider">
											<span class="bg-light px-4 position-absolute left-50pct top-50pct transform3dxy-n50">{"Je n'ai pas de compte"}</span>
										</div>
										<a href="#" class="btn btn-primary-scale-2 btn-modern w-100 text-transform-none rounded-0 font-weight-bold align-items-center d-inline-flex justify-content-center text-3 py-3" data-loading-text="Loading...">{"S'inscrire"}</a>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
        </AppBody>
     );
}
 
export default Login;