import React, { useEffect, useRef, useState } from 'react';
import AppBody from '../components/AppBody';
import {postResource} from '../config/Api'
import { toast, ToastContainer } from 'react-toastify';
import "react-toastify/dist/ReactToastify.css";
import { onServerSuccess, onServerError, storeData, validatorStringMinimum, validatorEmail, getData } from '../config/Helper';
import { useDispatch } from 'react-redux';
import {Icon} from 'react-icons-kit';
import {eyeOff} from 'react-icons-kit/feather/eyeOff';
import {eye} from 'react-icons-kit/feather/eye'
import Input from '../components/Input';
import { useNavigate, useParams } from 'react-router-dom';
import { saveData } from '../store/action/DataAction';

const Register = (props) => {

	const [errors, setErrors] = useState({});
	const [visible, setVisible] = useState(false)
	const [isValidPassword, setIsValidPassword] = useState(false)
	const [isValidPasswordConf, setIsValidPasswordConf] = useState(false)
	const [isValidEmail, setIsValidEmail] = useState(false)
	const [isValidName, setisValidName] = useState(false)
	const [isValidTel, setIsValidTel] = useState(false)
	 const dispatch = useDispatch()
	 const navigate = useNavigate()
	 const inputRef = useRef(null);
	const handleError = (error, input) => {
		setErrors(prevState => ({ ...prevState, [input]: error }));
	};
	const [isValid, setIsValid] = useState(false);
	const { parrain_id } = useParams();
	const [data, setData] = useState({
		name: "",
		email: "",
		sexe: "",
		parrain_id: parrain_id,
		birthday: "",
		phone: "",
		profession: "",
		country: "Benin",		
		password: "",
		password_confirmation: ""
		})
		const [icon, setIcon] = useState(eyeOff);


		 const handleToggle = () => {
			if (!visible){
				setVisible(true)
			   setIcon(eye);
			} else {
				setVisible(false)
			   setIcon(eyeOff);
			}
		 }

		 useEffect(() => {
		 }, [])

		 const handleInput = (e) => {
			const { name, value } = e.target;

			// Votre logique de validation ici, par exemple, vérifier la longueur minimale
				// if (value.length < 5) {
				// setIsValid(false);
				// } else {
				// setIsValid(true);
				// }

			// Validation du mot de passe
			if (name === 'password' && value.length > 8) {
			const regex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*])(?=.*[a-zA-Z]).{8,}$/;
			const isValidPassword = regex.test(value);
			console.log(isValidPassword)
			setIsValidPassword(isValidPassword);
				// if (isValidPassword) {
				// 	inputRef.current.isValid = true;
				// 	// Faites quelque chose avec la valeur de isValid dans le composant parent
				// 	console.log('isValid dans le composant parent:', inputRef.current.isValid);
				// }
			} 
			if(name === 'password_confirmation') {
				const isPasswordConfirmed = value === data.password;

				setIsValidPasswordConf(isPasswordConfirmed);
			}

				// validate(name, value)
			setData((s) => ({
			...s,
			[name]: value,
			}));
		};

		



	const validate = (event) => {
		// console.log("Ok")
         event.preventDefault()
        let isValid = true;
        
        if (!validatorStringMinimum(2, data.name)) {
			handleError("Votre nom et prenoms est obligatoire", 'name');
			isValid = false;
    	}

		if (!validatorStringMinimum(2,data.email)) {
			handleError("Votre email est obligatoire", 'email');
			isValid = false;
    	}

		if (!validatorStringMinimum(2,data.sexe)) {
			handleError("Votre sexe est obligatoire", 'sexe');
			isValid = false;
    	}

		if (!validatorStringMinimum(8, data.password)) {
		handleError("Votre mot de passe doit contenir minimum de 8 caractères. ", 'password');
		isValid = false;
		}
			if (!validatorStringMinimum(8, data.password_confirmation)) {
		handleError("Votre mot de passe doit contenir minimum de 8 caractères. ", 'password_confirmation');
		isValid = false;
		}

        if (isValid) {
        register()
    }


    }

	const encrypt = (data) => {
		var CryptoJS = require("crypto-js");
		// Utiliser votre méthode de chiffrement ici (ex: crypto-js)
		// Dans cet exemple, on utilise MD5 comme méthode de chiffrement
		const encrypted = CryptoJS.MD5(data).toString();
		return encrypted;
	  };

	const register = () => {
		console.log(errors, data)
		postResource('/register', data).then(res => {
			
			const access_token = res.data["hydra:member"].access_token
			const user = res.data["hydra:user"]
			console.log(access_token, user)
			// Chiffrer les données
			const encryptedData = encrypt(JSON.stringify(user));
			dispatch(saveData(encryptedData))
			storeData('access_token', access_token)
			onServerSuccess("Connexion reussie !!!")
			navigate('/profile')
		}).catch ((e) => {
            onServerError(e.message) 
        }).finally(() => {
        })
	}


    return ( 
        <AppBody shop>
            <div className="container py-4">

					<div className="row justify-content-center">
						
						<div className="col-md-6 col-lg-6 box-content">
							<h2 className="font-weight-bold text-5 mb-0">{"S'inscrire"}</h2>
							<form  id="frmSignUp" onSubmit={(e) => validate(e)}>
								<div className="row">
								<Input
								label="Nom et Prénom(s)"
								type="text" 
								name="name"
								lenght={data.name.length} 
								value={data.name} 
								onChange={handleInput} 
								className="form-control form-control-lg text-4"
								errors={errors} 
								required/>
									
                            </div>
                            <div className="row">
								<Input
								label="Email" 
								type="email"
								lenght={data.email.length} 
								name="email" 
								value={data.email} 
								onChange={handleInput} 
								className="form-control form-control-lg text-4" 
								required />
							</div>
                                <div className="row">
									<div className="form-group col">
										<label className="form-label text-color-dark text-3">Sexe<span className="text-color-danger">*</span></label>
										<div className="custom-select-1 mb-3">
											<select name="sexe" value={data.sexe} onChange={handleInput} className="form-select form-control h-auto py-2">
												<option value="">Choisir votre sexe</option>
												<option value="Masculin">Masculin</option>
												<option value="Feminin">Feminin</option>
											</select>
										</div>
									</div>
								</div>
                                <div className="row">
									
										<Input
										label="Date de naissance" 
										type="date"
										lenght={data.birthday.length} 
										name="birthday" 
										value={data.birthday} 
										onChange={handleInput} 
										className="form-control form-control-lg text-4" 
										required />
								</div>
                                <div className="row">
									<Input
									label="Telephone" 
									type="tel" 
									name="phone"
									lenght={data.phone.length} 
									value={data.phone} 
									onChange={handleInput} 
									className="form-control form-control-lg text-4" 
									required />
								</div>
								<div className="row">
									<Input
									label="Profession" 
									type="text"
									lenght={data.profession.length} 
									name="profession" 
									value={data.profession} 
									onChange={handleInput} 
									className="form-control form-control-lg text-4" 
									required />
								</div>
								<div className="row">
									<Input
									label="Code de Parrainage" 
									type="text"
									lenght={data.parrain_id.length} 
									name="parrain_id" 
									value={data.parrain_id} 
									onChange={handleInput} 
									className="form-control form-control-lg text-4" 
									required />
								</div>
								<div className="row">
										<Input
										label="Mot de Passe" 
										type={visible ? "text" : "password"}
										isValid={isValidPassword} 
										name="password" 
										value={data.password} 
										onChange={handleInput} 
										lenght={data.password.length}
										className="form-control form-control-lg text-4"
										handleToggle={handleToggle}
										icon={icon} 
										password
										required />
								</div>
                                <div className="row">
										
										<Input
										conf={true} 
										label="Confirmer mot de passe"
										type={visible ? "text" : "password"}
										isValid={isValidPasswordConf} 
										name="password_confirmation" 
										value={data.password_confirmation} 
										lenght={data.password_confirmation.length}
										onChange={handleInput}
										handleToggle={handleToggle} 
										className="form-control form-control-lg text-4"
										icon={icon}
										password 
										required />
								</div>
								<div className="row">
									<div className="form-group col">
										<p className="text-2 mb-2">Vos données personnelles seront utilisées pour soutenir votre expérience sur ce site Web, pour gérer l'accès à votre compte et à d'autres fins décrites dans notre  <a href="#" className="text-decoration-none">politique de confidentialité.</a></p>
									</div>
								</div>
								<div className="row">
									<div className="form-group col">
                                    <button type="button" onClick={(e) => validate(e)} className="btn btn-dark btn-modern w-100 text-uppercase rounded-0 font-weight-bold text-3 py-3" data-loading-text="Loading...">{ "S'inscrire"}</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
        </AppBody>
     );
}
 
export default Register;