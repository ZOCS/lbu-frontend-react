import CheckoutComponent from "../components/CheckoutComponent";
import AppBody from "../components/AppBody";
import { Link, useParams, useLocation } from "react-router-dom";
import { useEffect, useState } from "react";
import { getResource } from "../config/Api";
import { dateToFR, dateToFr, onServerError } from "../config/Helper";

const Confirm = () => {
	const [info, setInfo] = useState({})
	const [products, setProducts] = useState([])
	const {order} = useParams()

	const _init_ = async( ) => {
		await getResource(`/orders/${order}`).then(res => {
			console.log(res.data.order.products)
			setInfo(res.data.order)
			setProducts(res.data.order.products)
		}).catch((e) => {
			if(e.response.status === 500){
				onServerError(e.response.message)

			}else if(e.response.status === 404){
				onServerError(e.response.message)

			}
		})
	}
	useEffect(() => {
		_init_()
		console.log(order, info)
	}, [])

    return ( 
        <AppBody shop >
            <div className="container">

					<div className="row justify-content-center">
						<div className="col-lg-8">
							<ul className="breadcrumb breadcrumb-dividers-no-opacity font-weight-bold text-6 justify-content-center my-5">
								<li className="text-transform-none me-2">
									<Link to="/cart" className="text-decoration-none text-color-dark text-color-hover-primary">Panier</Link>
								</li>
								<li className="text-transform-none text-color-dark me-2">
									<Link to="/checkout" className="text-decoration-none text-color-dark text-color-hover-primary">Caisse</Link>
								</li>
								<li className="text-transform-none text-color-dark">
									<Link to="/confirm" className="text-decoration-none text-color-primary">Commande terminée</Link>
								</li>
							</ul>
						</div>
					</div>

					<div className="row justify-content-center">
						<div className="col-lg-8">
							<div className="card border-width-3 border-radius-0 border-color-success">
								<div className="card-body text-center">
									<p className="text-color-dark font-weight-bold text-4-5 mb-0"><i className="fas fa-check text-color-success me-1"></i> Merci. Votre commande a été reçue.</p>
								</div>
							</div>
							<div className="d-flex flex-column flex-md-row justify-content-between py-3 px-4 my-4">
								<div className="text-center">
									<span>
									Numéro de commande <br/>
										<strong className="text-color-dark">{info.code}</strong>
									</span>
								</div>
								<div className="text-center mt-4 mt-md-0">
									<span>
										Date <br/>
										<strong className="text-color-dark">{dateToFr(info.created_at)}</strong>
									</span>
								</div>
								<div className="text-center mt-4 mt-md-0">
									<span>
										Email <br/>
										<strong className="text-color-dark">{info.email}</strong>
									</span>
								</div>
								<div className="text-center mt-4 mt-md-0">
									<span>
										Total <br/>
										<strong className="text-color-dark">{info.prix} Fcfa</strong>
									</span>
								</div>
								<div className="text-center mt-4 mt-md-0">
									<span>
									Mode de paiement <br/>
										<strong className="text-color-dark">Cash on Delivery</strong>
									</span>
								</div>
							</div>
							<div className="card border-width-3 border-radius-0 border-color-hover-dark mb-4">
								<div className="card-body">
									<h4 className="font-weight-bold text-uppercase text-4 mb-3">Votre commande</h4>
									<table className="shop_table cart-totals mb-0">
										<tbody>
											<tr>
												<td colspan="2" className="border-top-0">
													<strong className="text-color-dark">Produits</strong>
												</td>
											</tr>
											{products.map(p => (<tr>
												<td>
													<strong className="d-block text-color-dark line-height-1 font-weight-semibold">{p.name} - <span className="product-qty">x{p.pivot.quantity}</span></strong>
													<span className="text-1">COLOR BLACK</span>
												</td>
												<td className="text-end align-top">
													<span className="amount font-weight-medium text-color-grey">{p.price_regular} Fcfa</span>
												</td>
											</tr>))}
											
											<tr className="cart-subtotal">
												<td className="border-top-0">
													<strong className="text-color-dark">Sous-Total</strong>
												</td>
												<td className="border-top-0 text-end">
													<strong><span className="amount font-weight-medium">{info.prix} Fcfa</span></strong>
												</td>
											</tr>
											<tr className="shipping">
												<td className="border-top-0">
													<strong className="text-color-dark">Shipping</strong>
												</td>
												<td className="border-top-0 text-end">
													<strong><span className="amount font-weight-medium">Free Shipping</span></strong>
												</td>
											</tr>
											<tr className="total">
												<td>
													<strong className="text-color-dark text-3-5">Total</strong>
												</td>
												<td className="text-end">
													<strong className="text-color-dark"><span className="amount text-color-dark text-5">{info.prix} Fcfa</span></strong>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<div className="row pt-3">
								<div className="col-lg-6 mb-4 mb-lg-0">
									<h2 className="text-color-dark font-weight-bold text-5-5 mb-1">Adresse de facturation</h2>
									<ul className="list list-unstyled text-2 mb-0">
										<li className="mb-0">{info.name}</li>
										<li className="mb-0">{info.adresse}</li>
										{/* <li className="mb-0">State AL 85001</li> */}
										<li className="mb-0">{info.phone}</li>
										<li className="mt-3 mb-0">{info.email}</li>
									</ul>
								</div>
								<div className="col-lg-6">
									<h2 className="text-color-dark font-weight-bold text-5-5 mb-1">Adresse de livraison</h2>
									<ul className="list list-unstyled text-2 mb-0">
										<li className="mb-0">John Doe Junior</li>
										<li className="mb-0">Street Name, City</li>
										<li className="mb-0">State AL 85001</li>
										<li className="mb-0">123 456 7890</li>
										<li className="mt-3 mb-0">abc@abc.com</li>
									</ul>
								</div>
							</div>
							<Link to="/shop" className="btn btn-dark btn-modern w-100 text-uppercase bg-color-hover-primary border-color-hover-primary border-radius-0 text-3 py-3">Aller a la Boutique <i className="fas fa-arrow-right ms-2"></i></Link>
						</div>
					</div>

				</div>
        </AppBody>
     );
}
 
export default Confirm;