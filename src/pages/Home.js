/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
import AppBody from "../components/AppBody";
import { getResource } from "../config/Api";
import { onServerError } from "../config/Helper";
import Loading from "../components/Loading";
import CardProduct from "../components/CardProduct";

const Home = () => {
  const [product, setProduct] = useState([]);
  const [loading, setLoading] = useState(false);

  const products = () => {
    getResource(`products`)
      .then((res) => {
        setLoading(true);
        console.log(res.data);
        setProduct(res.data["data"]);
        setLoading(false);
      })
      .catch((e) => {
        onServerError(e.message);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  
  useEffect(() => {
    products();
  }, []);
  return (
    <AppBody slide>
      {/* <div className="container"> */}
      <div id="examples" className="container py-2">
        <div className="row">
          <div className="col">
            <h2
              className="text-start"
              style={{ fontWeight: 700, fontSize: 30 }}
            >
              Nouveaux Articles
            </h2>
            <div
              className="owl-carousel owl-theme show-nav-title show-nav-title-both-sides owl-loaded owl-drag owl-carousel-init"
              data-plugin-options="{'items': 6, 'margin': 5, 'loop': false, 'nav': true, 'dots': false}"
            >
              {loading ? (
                <Loading />
              ) : (
                product.map((x) => <CardProduct item={x} />)
              )}
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col">
            <hr className="solid my-5" />
          </div>
        </div>
        <div className="row">
          <div className="col">
            <h2
              className="text-start"
              style={{ fontWeight: 700, fontSize: 30 }}
            >
              Articles Scrolaires
            </h2>
            <div
              className="owl-carousel owl-theme show-nav-title show-nav-title-both-sides owl-loaded owl-drag owl-carousel-init"
              data-plugin-options="{'items': 6, 'margin': 5, 'loop': false, 'nav': true, 'dots': false}"
            >
              {loading ? (
                <Loading />
              ) : (
                product.map((x) => <CardProduct item={x} />)
              )}
            </div>
          </div>
        </div>
		<div className="row">
          <div className="col">
            <hr className="solid my-5" />
          </div>
        </div>
		<div className="row">
          <div className="col">
            <h2
              className="text-start"
              style={{ fontWeight: 700, fontSize: 30 }}
            >
              Sacs et Chaussures
            </h2>
            <div
              className="owl-carousel owl-theme show-nav-title show-nav-title-both-sides owl-loaded owl-drag owl-carousel-init"
              data-plugin-options="{'items': 6, 'margin': 5, 'loop': false, 'nav': true, 'dots': false}"
            >
              {loading ? (
                <Loading />
              ) : (
                product.map((x) => <CardProduct item={x} />)
              )}
            </div>
          </div>
        </div>
      </div>
      {/* </div> */}
    </AppBody>
  );
};

export default Home;
