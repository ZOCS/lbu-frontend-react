import CheckoutComponent from "../components/CheckoutComponent";
import AppBody from "../components/AppBody";

const Checkout = () => {
    return ( 
        <AppBody shop >
        <CheckoutComponent/>
        </AppBody>
     );
}
 
export default Checkout;