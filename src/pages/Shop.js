import AppBody from '../components/AppBody';
import ShopComponent from '../components/ShopComponent'
import "react-toastify/dist/ReactToastify.css";


const Shop = () => {
    return ( 
        <AppBody shop clas="shop pt-4">
        
            <ShopComponent/>
        </AppBody>
     );
}
 
export default Shop;