import AppBody from "../components/AppBody";
import SingleProduct from "../components/SingleProduct";

const Single = () => {
    return ( 
        <AppBody shop >
        <SingleProduct/>
        </AppBody>
     );
}
 
export default Single;