import AppBody from "../components/AppBody";
import CartComponent from "../components/CartComponent";

const Cart = () => {
    return ( 
        <AppBody shop clas="shop pt-4">
          <CartComponent/>
        </AppBody>
     );
}
 
export default Cart;