/* eslint-disable no-undef */
import { initializeApp } from "firebase/app";
import { getMessaging, getToken } from "firebase/messaging";
import "https://www.gstatic.com/firebasejs/9.1.3/firebase-app-compat.js";
import "https://www.gstatic.com/firebasejs/9.1.3/firebase-messaging-compat.js"; 
const firebaseConfig = {
    apiKey: "AIzaSyCPE_u2xVFO-csG2LDe3oivyvImRmDgRBg",
    authDomain: "lbu-app.firebaseapp.com",
    projectId: "lbu-app",
    storageBucket: "lbu-app.appspot.com",
    messagingSenderId: "926708914216",
    appId: "1:926708914216:web:36e7a990da66b0e8393d1d",
    measurementId: "G-E9E3L889TG"
};

  function requestPermission() {
    console.log('Requesting permission...');
    Notification.requestPermission().then((permission) => {
      if (permission === 'granted') {
        console.log('Notification permission granted.');
        // Initialize Firebase
        const app = initializeApp(firebaseConfig);


        // Initialize Firebase Cloud Messaging and get a reference to the service
        const messaging = getMessaging(app);

        // Add the public key generated from the console here.
        getToken(messaging, {vapidKey: "BJCLu_UTU1OyPXIUxFvMN6Gcy1Gj5Fhp6TfLTZYwjbUlfrXl7ORpdbNRjBcfPgLUTjrDVgHtF9CsQV3U5MyylTw"}).then((currentToken) => {
            if (currentToken) {
              console.log('currentToken: ', currentToken);
            } else {
              // Show permission request UI
              console.log('No registration token available. Request permission to generate one.');
              // ...
            }
          }).catch((err) => {
            console.log('An error occurred while retrieving token. ', err);
            // ...
          });
      }else{
        console.log('Do not have permission')
      }
    });
  }

  requestPermission();

