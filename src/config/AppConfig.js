import React from 'react'

 const AppConfig = {
    appName: "Librairie LBU",
    apiUrl: "https://api-librairielbu.apaid-benin.org",
    apiUrlLocal: "http://api-librairielbu.test",
    apiUrlWebLocal: "http://api-librairielbu.test/storage",
    apiUrlWeb: "https://api-librairielbu.apaid-benin.org/storage",
    
}

export default AppConfig;