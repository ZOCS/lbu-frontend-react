export const saveData = (data) => ({
    type: 'SAVE_DATA',
    payload: data,
  });
  