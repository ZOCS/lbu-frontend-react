// For add Item to Cart

export const addCart = (product) => {
    return {
        type: "ADDITEM",
        payload: product
    }
}



// For Delete Item From Cart
export const delCart = (product) => {
    return {
        type: "DELITEM",
        payload: product
    }
}

export const resetCart = () => {
    return {
        type: "RESETCART",
    }
}

export const resetProduct = (product) => {
    return {
        type: "RESETPRODUCT",
        payload: product
    }
}