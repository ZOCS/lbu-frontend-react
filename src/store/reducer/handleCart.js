const cart = [];


const handleCart = (state = cart, action) => {
    const product = action.payload;

    switch (action.type) {
        case "ADDITEM":
            const exist = state.find((x) => x.id === product.id);
            if (exist) {
                
                // Increase the Quantity
                return state.map((x) => x.id === product.id ? { ...x, qty: x.qty + 1, totalPrice: (x.qty + 1) * x.unitPrice }: x);
            } else {
                const product = action.payload;
                return [
                    ...state, {
                        ...product, qty: 1,totalPrice: product.unitPrice
                    }
                ]
            }
            break;

        case "DELITEM":
            const exist1 = state.find((x) => x.id === product.id);
            if (exist1.qty === 1) {
                return state.filter((x) => x.id !== exist1.id);
            } else {
                return state.map((x) => x.id === product.id ? { ...x, qty: x.qty - 1, totalPrice: (x.qty - 1) * x.unitPrice } : x);
            }
            break;
        case "RESETPRODUCT":
             // Supprimer le produit du panier en le filtrant
            return state.filter((item) => item.id !== product.id);
        case "RESETCART":
            return cart;
            break;
        default:
           return state;
    }
}

export default handleCart;