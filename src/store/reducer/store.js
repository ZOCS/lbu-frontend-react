import { createStore } from 'redux';
import { configureStore, applyMiddleware } from '@reduxjs/toolkit';
import thunk from "redux-thunk"
import rootReducers from './reducer';
import { persistStore, persistReducer } from 'redux-persist';
import storage from "redux-persist/lib/storage";
import DataReducer from './DataReducer';




const persistConfig = {
  key: 'root',
  storage,
};

const persistedReducer = persistReducer(persistConfig, rootReducers);

export const store = configureStore(
  {
    reducer: {
      root: persistedReducer,
      data: DataReducer,
    },
  },
  applyMiddleware(thunk)
);


export const persistor = persistStore(store);